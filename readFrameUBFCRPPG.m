function frame = readFrameUBFCRPPG(obj, varargin)
%READFRAME Read the next available frame from a UBFC-RPPG dataset or a
% regular video file


% Copyright 2018-2019 Université de Bourgogne.


if isa(obj,'UBFCRPPGVideoReader')
    if isempty(obj.VideoReader)
        currentFrame = dir([obj.path 'frame_' num2str(obj.currentFrameNumber-1)...
            '_*.' obj.Format]);
        obj.currentFrame = imread([obj.Path currentFrame.name]);
        frame = obj.currentFrame;
        obj.currentFrameNumber = obj.currentFrameNumber + 1;
    else
        frame = readFrame(obj.VideoReader);
    end
end