function launcherSrPDEV(overwrite)
%LAUNCHERSRPDEV Run SrPDEV on all fixedData20122017

fixedDataFolder = 'F:\LabData\fixedData20122017\';
folders = dir(fixedDataFolder);
%ignoreList = {'2017_12_20-14_55_23', '2017_12_20-15_02_30','2017_12_20-15_06_34'};
for i=1:size(folders,1)
    skip = false;
    if ~strcmp(folders(i).name,'.') && ~strcmp(folders(i).name,'..') 
        rppgOut = dir([fixedDataFolder folders(i).name '\rppgOut.avi']);
        if ~overwrite && ~isempty(rppgOut) && rppgOut.bytes ~=0
            fprintf('%s already processed, skipping... \n',folders(i).name);
            skip = true;
        end
        if(overwrite || ~skip)
            d = folders(i);
            fprintf('SrPDEV on %s%s\\ \n',fixedDataFolder,d.name);
            SrPDEV('vidFolder', [fixedDataFolder d.name '\']);
        end
    end
end
end

