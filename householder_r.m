function [E,F] = householder_r(E,F,r,k,col)
%%Find householder reflections to eliminate elements k+1:k+r-1 of column
%%col
normx = norm(E(k:k+r-1,col));
% normx = sqrt( sum(E(k:k+r-1,col).^2));
s = -sign(E(k,col));
u1 = E(k,col) - s*normx;
%f = [u1; E(k+1:k+r-1,col)];

w = E(k:k+r-1,col)/u1;
w(1) = 1;
tau = -s*u1/normx;
%tau = 2/(norm(f).^2);
% E := Qk*E
E(k:k+r-1,:) = E(k:k+r-1,:)-(tau*w)*(w'*E(k:k+r-1,:));
% F := Qk*F
F(k:k+r-1,:) = F(k:k+r-1,:)-(tau*w)*(w'*F(k:k+r-1,:));
end
