function [PPGlag, PPG_peaksLoc] = getTauFromPPG(gtTimeWin,gtTraceWin, Fs_PPG)
[PPGlag,PPG_peaksLoc, ~, ~] = fftPPG(gtTimeWin, gtTraceWin, Fs_PPG);
end
