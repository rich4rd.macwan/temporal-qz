function [rPPG_freq,rPPG_power, rPPG_peaksLoc, handles] = fftrPPG(x, Fs, varargin)
% Compute fft of spatio-temporal rPPG signal
%The last two optional args are nLags = 0 (default) and noplot = True|FALSE.
% handles : Structure containing handles to subplots in the parent display.
% e.g. in SrPDEVplayer.m
% noplot: switch display of plots on or off
% x should be a row vector or a matrix of row wise rPPG signals
noplot = false;
handles =[];
i=1;
meanTrace=[];
while i + 2 < nargin
    if strcmp(varargin{i},'noplot')==1
        noplot= varargin{i+1};
    end
    if strcmp(varargin{i},'handles')==1
        handles= varargin{i+1};
    end
    if strcmp(varargin{i},'meanTrace')==1
        meanTrace= varargin{i+1};
    end
    i=i+2;
end

% Frequency limits corresponding to the human HR
LOW_F = .7; UP_F = 3;
N = length(x)*3;
rPPG_freq = (0 : N-1)*Fs/N;
%If a matrix of rPPG pulses is passed, calculate the fft all at once
sz = size(x);
nx = numel(x);
rangeRPPG = (rPPG_freq>LOW_F & rPPG_freq < UP_F);   % frequency range to find PSD peak
rPPG_freq = rPPG_freq(rangeRPPG);
matrixMode = false;
valid_rPPG_peaks = [];
if(nx==sz(1) || nx ==sz(2))
    matrixMode = false;
    %row or column vector
    rPPG_power = abs(fft(x,N)).^2;
    % Get peaks rPPG
    rPPG_power = rPPG_power(rangeRPPG);
    if numel(rPPG_power)>3
        [pksrPPG,loc] = findpeaks(rPPG_power,'SortStr','descend','NPeaks',2); % get 2 first peaks (only one is used now but will be useful for postpreocessing
        rPPG_peaksLoc = rPPG_freq(loc);
    else
        rPPG_peaksLoc=[];
        pksrPPG = [];
    end
else
    matrixMode = true;
    %matrix, preferrably row wise
    rPPG_power = abs(fft(x,N,2)).^2;
    %rangeRPPG = (rPPG_freq>LOW_F & rPPG_freq < UP_F);   % frequency range to find PSD peak
    rPPG_power = rPPG_power(:,rangeRPPG);
    rPPG_peaksLoc = zeros(size(rPPG_power,1),2);
    rPPG_peaks = zeros(size(rPPG_power,1),2);
    for i=1:size(rPPG_power,1)
        if ~isnan(rPPG_power(i,1))
            rPPG_poweri = rPPG_power(i,:);
            %rPPG_poweri = rPPG_poweri(rangeRPPG);
            [pksrPPG,loc] = findpeaks(rPPG_poweri ,'SortStr','descend','NPeaks',2); % get 2 first peaks (only one is used now but will be useful for postpreocessing
            if(~isempty(rPPG_freq(loc)))
                rPPG_peaksLoc(i,:) = rPPG_freq(loc);
                rPPG_peaks(i,:) = pksrPPG;
            end
        end     
    end
    
    valid_rPPG_peaks = rPPG_peaksLoc(:,1)~=0;
    %For mean trace
    if ~isempty(meanTrace)
        rPPG_power_mean = abs(fft(meanTrace,N)).^2;
        rPPG_power_mean  = rPPG_power_mean(rangeRPPG);
        [pksrPPG_mean,loc_mean] = findpeaks(rPPG_power_mean,'SortStr','descend','NPeaks',2);        
    end
end



% fig5 = figure(5);
% clf(fig5);
if(~noplot)
    %subplot(2,3,3);
    %     cla;
    %     hold on;grid on;
    if ~matrixMode
        if isfield(handles,'h326')
            set(handles.h326.p1,'XData',rPPG_freq,'YData',rPPG_power);
            if ~isempty(pksrPPG)
                set(handles.h326.p2,'XData',rPPG_peaksLoc(1),'YData',pksrPPG(1));
            end
        else
             handles.h326.p1=plot(handles.s326,rPPG_freq,rPPG_power);
            if ~isempty(pksrPPG)
                handles.h326.p2=plot(handles.s326,rPPG_peaksLoc(1), pksrPPG(1), 'r*');
            end
        end
         yl = ylim(handles.s326);
        %t=text(1.5, yl(2)/2 ,[num2str(round(rPPG_peaksLoc(1)*60)) 'bpm  --- ' num2str(nLags)]);
        if isfield(handles,'h326')
            if isfield(handles.h326,'t')
                delete(handles.h326.t);
            end
        end
        if ~isempty(rPPG_peaksLoc)
            handles.h326.t=text(handles.s326,1.5, yl(2)/2 ,[num2str(round(rPPG_peaksLoc(1)*60)) 'bpm']);
        end
    else
            if ~isfield(handles,'hpanel2')
                % Plot fftRPPG per pixel
                markersize=26; linewidth = 3;
                handles.hpanel2.p1 = mesh(handles.spanel2,repmat(rPPG_freq',1,size(rPPG_power,1)), repmat(1:size(rPPG_power,1),size(rPPG_power,2),1),rPPG_power');   
                set(handles.hpanel2.p1,'FaceAlpha',.4);
                set(handles.hpanel2.p1,'EdgeAlpha',.4);
                %Peaks of pixelwise fft
                handles.hpanel2.p2 = plot3(handles.spanel2,rPPG_peaksLoc(valid_rPPG_peaks,1),1:numel(find(valid_rPPG_peaks)),rPPG_peaks(valid_rPPG_peaks,1),'.','MarkerSize',markersize);
                handles.hpanel2.p2_5 = plot3(handles.spanel2,rPPG_peaksLoc(valid_rPPG_peaks,1),1:numel(find(valid_rPPG_peaks)),rPPG_peaks(valid_rPPG_peaks,1));
                %Mean of FFT
                mean_rPPG_power = nanmean(rPPG_power);
                [pksrPPG,loc] = findpeaks(mean_rPPG_power,'SortStr','descend','NPeaks',2); 
                handles.hpanel2.p3 = plot3(handles.spanel2,rPPG_freq, zeros(1,size(rPPG_power,2)), mean_rPPG_power ,'LineWidth',linewidth);
                handles.hpanel2.p4 = plot3(handles.spanel2,rPPG_freq(loc(1)),0,pksrPPG(1),'.','MarkerSize',markersize);
                
                %Get color to indicate legend in title
                clr = get(handles.hpanel2.p3,'Color');
                % FFT of mean pulse Trace
                handles.hpanel2.p5 = plot3(handles.spanel2,rPPG_freq,-1*ones(1,size(rPPG_power,2)),rPPG_power_mean,'LineWidth',linewidth);
                clr2 = get(handles.hpanel2.p5,'Color');
                if ~isempty(pksrPPG_mean)
                    handles.hpanel2.p6 = plot3(handles.spanel2,rPPG_freq(loc_mean(1)),-1,pksrPPG_mean(1),'.','MarkerSize',markersize);
                end
                title(['Spatial rPPG FFT, \color[rgb]{' num2str(clr) ...
                    '}mean(FFT) and \color[rgb]{' num2str(clr2) '}FFT(meanPulseTrace)']);
                view(handles.spanel2,26,26);
            else
                set(handles.hpanel2.p1,'XData',repmat(rPPG_freq',1,size(rPPG_power,1)),'YData',repmat(1:size(rPPG_power,1),size(rPPG_power,2),1),...
                'ZData',rPPG_power');
                set(handles.hpanel2.p2,'XData',rPPG_peaksLoc(valid_rPPG_peaks,1),'YData',1:numel(find(valid_rPPG_peaks)),'ZData',rPPG_peaks(valid_rPPG_peaks,1));
                set(handles.hpanel2.p2_5,'XData',rPPG_peaksLoc(valid_rPPG_peaks,1),'YData',1:numel(find(valid_rPPG_peaks)),'ZData',rPPG_peaks(valid_rPPG_peaks,1));
                mean_rPPG_power = nanmean(rPPG_power);
                [pksrPPG,loc] = findpeaks(mean_rPPG_power,'SortStr','descend','NPeaks',2); 
                set(handles.hpanel2.p3,'XData',rPPG_freq,'YData',zeros(1,numel(rPPG_freq)),'ZData',mean_rPPG_power);
                if ~isempty(loc)
                    set(handles.hpanel2.p4,'XData',rPPG_freq(loc(1)),'YData',0,'ZData',pksrPPG(1));
                end
            end
            
            
    end
   
end
end
