function [rPPG_freq,rPPG_power, rPPG_peaksloc] = fftrPPG_RGB(RGB, Fs, nLags)
%Compute fft of 1XN rPPG signal
[PM, y, K, R, bestD] = estimatePeriodicityMetric(RGB,nLags);

[rPPG_freq,rPPG_power, rPPG_peaksloc,~] = fftrPPG(y, Fs, 'noplot',true);


end
