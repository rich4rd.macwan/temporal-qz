function [x, PMs, bestWs] = tabusearch(X_full, LB, UB, Fs)
brute = 1;
stepScale = 1;%Search for each .5 bpm

taus = 60/UB:1/Fs:60/LB;
%candidates = LB:stepScale:UB;
candidates = int32(Fs*taus);
allTaus = 60./(fliplr(candidates));
HRs = 60./fliplr(taus);
LB = candidates(1); UB = candidates(end);
bestWs = [];
if brute == 1
    bestPM = 0;
    PMs = zeros(size(candidates));
    %PMs = zeros(size(HRs));
    ind = 1;
    for candidate = candidates
    %for HR = HRs
        [~, ~, indicesWithData] = intersect(taus, allTaus);
        if ismember((candidate-LB)/stepScale + 1,indicesWithData)
            interpolateX = false;
        else
            interpolateX = true;
        end
        assignin('base','interpolateX',interpolateX);
%        assignin('base','interpolateX',false);
        [fval, y,~,~, bestW] = estimatePeriodicityMetric(X_full,candidate);
%         if winIndex == 9
%             plot(y);
%             title(num2str(candidate));
%             pause(.3);
%             
%         end
        PMs(ind) = fval;
        %bestWs(ind) = bestW;
%         if fval > bestPM
%             bestPM = fval;
%             x = candidate;
%             besty = y;
%         end
        ind = ind + 1;
    end
    %plocs = findPeaks(PMs);
    [pks, locs]=findpeaks(PMs,double(candidates));
    [bestPM i] = max(pks);
    x = locs(i);
    %x = plocs + candidates(1);
  %  disp([ 'Finished brute search']);
else
    %Customized Tabu search algorithm to optimize in [LB:UB], using integer
    %values only
    targetPM = 85;
    %Choose a random HR between 60 and 90 bpm as a starting point.
    idx = randi([11 20]);
    sBest = candidates(idx); %lag corresponding to 60bpm
    bestCandidate = sBest;
    tabuList = sBest;
    
%     [~, ~, indicesWithData] = intersect(taus, allTaus);
%     if ismember((sBest-LB)/stepScale + 1,indicesWithData)
%         interpolateX = false;
%     else
%         interpolateX = true;
%     end
%     assignin('base','interpolateX',interpolateX);
    interpolateX = false;
    
    fitness = zeros(size(candidates));
    n = 1;
    fval = 0;
    
    %Init
    [fval, y] = estimatePeriodicityMetric(X_full, sBest);
    fitness((sBest - LB)/stepScale + 1) = fval;
    quitFlag = false;
    
    while n<numel(candidates)
        neighbors = getNeighbors(bestCandidate,tabuList,[LB UB], stepScale);
        if isempty(neighbors)
            %Exhausted all search candidates
            break;
        end
        for sCandidate = neighbors
%             if ismember((sCandidate-LB)/stepScale + 1 ,indicesWithData)
%                 interpolateX = false;
%             else
%                 interpolateX = true;
%             end
%             assignin('base','interpolateX',interpolateX);
            if fitness((sCandidate- LB)/stepScale + 1) ~= 0 %Already calculated
                fval = fitness((sCandidate- LB)/stepScale + 1);
            else
                [fval, y] = estimatePeriodicityMetric(X_full,sCandidate);
                if fval > targetPM
                    sBest = sCandidate;
                    quitFlag = true;
                end
            end
            fitness((sCandidate - LB)/stepScale + 1) = fval;
            if fitness((sCandidate - LB)/stepScale + 1) > fitness((bestCandidate - LB)/stepScale + 1)
                bestCandidate = sCandidate;
            end
            
            %Add the new members to tabuList in a sorted manner
            if sCandidate < tabuList(1)
                tabuList = [sCandidate tabuList];
            else
                tabuList = [tabuList sCandidate];
            end
        end
        if fitness((bestCandidate - LB)/stepScale + 1) > fitness((sBest - LB)/stepScale + 1)
            sBest = bestCandidate;
        end
        if quitFlag
            break;
        end
        
        n = n+1;
    end
    x = sBest;
   % disp([ 'Finished in ' num2str(n) ' iterations']);
end


end

function neighbors = getNeighbors(bestCandidate,tabuList, bounds, stepScale)
v = int32(stepScale*[-1 1]);
neighbors = bestCandidate + v;
for i=1:numel(neighbors)
    while ~(neighbors(i)<tabuList(1)) && ~(neighbors(i)>tabuList(end))
        neighbors(i) = neighbors(i) + v(i);
    end
end
%Check that we have not exceeded the bounds
neighbors(neighbors<bounds(1)) = [];
neighbors(neighbors>bounds(2)) = [];

end