function [bigA, bigB,alfr,alfi,beta,X,V] = qz_temporal(bigA,bigB,wantx)
S2 = size(bigA,3);
X = repmat(eye(size(bigA,1)),1,1,S2);
%tic
[bigA,bigB,X]=qzhes(bigA,bigB,wantx,X,'S',sqrt(S2),'vectorize', true);
%toc
[bigA,bigB,epsa,epsb,X]=qzit_temporal(bigA,bigB,1e-10,wantx,X);
[bigA,bigB,alfr,alfi,beta,X] = qzval_temporal(bigA,bigB,epsa,epsb,wantx,X);
if(wantx)
    [bigA, bigB,X] = qzvec_temporal(bigA, bigB, epsa,epsb,alfr,alfi,beta,X);
end
%Scale so that W'PxW = D and W'CxW=I
%X'*Cx*X
%s=mtimesx(mtimesx(X,'T',bigB),X,'N','SPEEDOMP','OMP_SET_NUM_THREADS(4)');
D = (alfr./beta);
%d is going to be 3xW*W
%d=(1./sqrt([s(1,1,:);s(2,2,:);s(3,3,:)]));
%Diagonal matrix tensor of d
%d = [ d(1,1,:), zeros(1,2,S2);
%      zeros(1,1,S2), d(2,1,:), zeros(1,1,S2);
%      zeros(1,2,S2),d(3,1,:)];
%V = mtimesx(X,d);
[~, indices] = sort(-1*abs(D));
%Rearrange D based on sort order
%D = D(indices);
D = D(sub2ind(size(D),indices,repmat((1:S2),3,1)));
%V = V(:,indices);
%Rearrange the columns of each 3x3 block. This works good. indices are
%reshaped into a 3x3xS2 tensor to fit the size of V
idx = sub2ind(size(X),repmat([1:3]',1,3,S2),repmat(reshape(indices,1,3,S2),3,1),(repmat(reshape(1:S2,1,1,S2),3,3,1)));
V=X(idx);
%Get the first eigenvector for each block, 1x3 times, no need to do W' for
%W'X
V = permute(V(:,1,:),[2 1 3]);
end