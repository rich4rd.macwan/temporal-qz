function [PPG_freq, PPG_power,PPG_peaksLoc, handles] = fftPPG(gtTimeWin, gtTraceWin, Fs_PPG, varargin)
% Computer fft of spatio-temporal PPG signal

% noplot (True|FALSE): switch display of plots on or off, Call subplot before this
% function to place it correctly
% handles : Structure containing handles to subplots in the parent display.
% e.g. in SrPDEVplayer.m
noplot = false;
handles =[];
i=1;
while i + 2 < nargin
    if strcmp(varargin{i},'noplot')==1
        noplot= varargin{i+1};
    end
    if strcmp(varargin{i},'handles')==1
        handles= varargin{i+1};
    end
    i=i+2;
end

% Frequency limits corresponding to the human HR
LOW_F = .7; UP_F = 3;
N = length(gtTimeWin )*3;
PPG_freq = (0 : N-1)*Fs_PPG/N;
PPG_power = abs(fft(gtTraceWin ,N)).^2;

rangePPG = (PPG_freq>LOW_F & PPG_freq < UP_F); % frequency range to find PSD peak
PPG_power = PPG_power(rangePPG);
PPG_freq = PPG_freq(rangePPG);
pksPPG=[];
PPG_peaksLoc=[];
if numel(PPG_power)>3
    [pksPPG,loc] = findpeaks(PPG_power,'SortStr','descend','NPeaks',2); % get 2 first peaks
    PPG_peaksLoc = PPG_freq(loc);
end

% if ~isempty(PPG_peaksLoc)
%     PPGlag = int32(Fs_PPG/PPG_peaksLoc(1));
% end

if(~noplot)
    if isfield(handles,'h325')
        set(handles.h325.p1,'XData',PPG_freq,'YData',PPG_power);
        if ~isempty(pksPPG)
            set(handles.h325.p2,'Xdata',PPG_peaksLoc(1),'YData',pksPPG(1));
        end
    else
         %subplot(2,3,2);
        handles.h325.p1 = plot(handles.s325,PPG_freq,PPG_power);
        if ~isempty(pksPPG)
            handles.h325.p2=plot(handles.s325,PPG_peaksLoc(1), pksPPG(1), 'r*');
        end
    end
   
    yl = ylim(handles.s325);
    if isfield(handles,'h325')
        if isfield(handles.h325,'t')
            delete(handles.h325.t);
        end
    end
    
    if ~isempty(PPG_peaksLoc)
        handles.h325.t=text(handles.s325,1.5, yl(2)/2 ,[num2str(round(PPG_peaksLoc(1)*60)) 'bpm']);
    end
    %t=text(1.5, yl(2)/2 ,[num2str(round(PPG_peaksLoc(1)*60)) 'bpm']);
    
    %set(t,'BackgroundColor',[.9 .1 .4 .1]);
    %xlim([0.1 3.5]), title('PPG Welch periodogram'), xlabel('Heart Rate (Hz)');
    %hold off;
end
end