function [E,F,X] = qzhes(E,F,wantx,X,varargin)
nFixedArgs = 4;
DO_BLOCK = false;
sz = size(E);
if sz(1)~=sz(2)
    error ('A must be square!');
end

S = 1; %Default step size
N = sz(1);
vectorize = false;
%Set up block parameters
if(nargin>nFixedArgs)
    for i = 1 : nargin-nFixedArgs
        DO_BLOCK = true;
        if strcmp(varargin(i),'S')
            S = varargin{i+1};
        end
        if strcmp(varargin(i),'N')
            N = varargin{i+1};
        end
        if strcmp(varargin(i),'vectorize')
            vectorize = varargin{i+1};
        end
    end
end
%Set n to appropriate size for both block and non-block versions
n = N;
%Verify block sizes are correct
if vectorize
    assert (N==sz(1) && N==sz(2),'Please pass appropriate block size parameters. For vectorized version, A is of size NxNx(S*S)');
else
    assert (N*S==sz(1),'Please pass appropriate block size parameters. N*S==n. If you want to use the block version, pass ''vectorize'' as false. ');
end
%% Householder reflections to reduce B to upper triangular


for k = 1:n-1
    % -- Find Qk = I-tau*w*w� to put zeros below E(k,k)
    r = n-k+1;
    if DO_BLOCK
        if vectorize
            [F,E]= householder_r_temporal(F,E,r,k,k);
        else
            [F,E]= householder_r_block(F,E,r,k,k,S,N);
        end
    else
        [F,E]= householder_r(F,E,r,k,k);
    end
end

%% Reduce A to upper Hessenberg form while preserving

for k = 1:n-2
    for l =n-1:-1:k+1
        %H2 ,pre
        if DO_BLOCK
            if vectorize
                [E,F]= householder_r_temporal(E,F,2,l,k);
            else
                [E,F]= householder_r_block(E,F,2,l,k,S,N);
            end
        else
            [E,F]= householder_r(E,F,2,l,k);
        end
        % Choose Zkl to annihilate zeros below F(k,k) introduced by the
        % annihilation of zeros below E(k,k)
        %H2 post
        if DO_BLOCK
            if vectorize
                [F, E, X] = householder_r_post_temporal(F,E,2,l,l,wantx,X);
            else
                [F, E, X] = householder_r_post_block(F,E,2,l,l,wantx,X,S,N);
            end
        else
            [F, E, X] = householder_r_post(F,E,2,l,l,wantx,X);
        end
        
    end
end
end