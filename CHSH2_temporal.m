function [c,sr,si]=CHSH2_temporal(a1r,a1i,a2r,a2i)
%Householder reflection for complex data, subroutine from origin QZ article
%C . B . Moler and G . W . Stewart, �An Algorithm for Generalized
%Matrix Eigenvalue Problems�
c = zeros(size(a1r));
sr = c;
si = c;

%For all a2r ==0 and a2i == 0 do this
mask1 = a2r==0 & a2i ==0;
c(mask1) = 1;
sr(mask1) = 0;
si(mask1) = 0;

%For all a1r ==0 and a1i == 0 do this, accumulate mask
mask2 = ~mask1 & (a1r==0 & a1i==0);
c(mask2) = 0;
sr(mask2) = 1;
si(mask2) = 0;

mask3 = ~mask2;
r = sqrt(a1r(mask3).*a1r(mask3)+a1i(mask3).*a1i(mask3));
c(mask3) = r;
sr(mask3) =(a1r(mask3).*a2r(mask3) + a1i(mask3).*a2i(mask3))./r;
si(mask3) =(a1r(mask3).*a2i(mask3) - a1i(mask3).*a2r(mask3))./r;
r = sqrt(c(mask3).*c(mask3)+sr(mask3).*sr(mask3)+si(mask3).*si(mask3));
c(mask3) = c(mask3)./r;
sr(mask3) = sr(mask3)./r;
si(mask3) = si(mask3)./r;

end