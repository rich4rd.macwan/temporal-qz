function [E,F,u1zeromask] = householder_r_block(E,F,r,k,col,S,N, varargin)
%NOTE: This function is deprecated, not maintained!Just here for backward
%compatibility
%%Find householder reflections to eliminate elements k+1:k+r-1 of column
%%col
%%SxS = block size and NxN is the number of blocks
%%Total size of matrix is SNxSN
%%All the calculations below will be done as if it were a matrix of NxN


nFixedArgs = 7;
mask = [];
if nargin >nFixedArgs
    for i = 1 : nargin-nFixedArgs
        if strcmp(varargin(i),'mask')
            mask = varargin{i+1};
            
        end
    end
end
oldE = E;
oldF = F;
ncols = size(E,2)/S;
%mask is only S*S, rep it to work with the whole matrices
fullmask = repmat(mask,[N ncols]);
%Block equivalent parameters
K = (k-1)*S+1;
R=S*r;
COL = (col-1)*S+1:col*S;

%normx = norm(E(k:k+r-1,col));
%Reshape the block column vector into a block column vector in the third
%dim
%https://www.dropbox.com/s/jmlv46ueig1bjex/reshapeForNormXHouseholder.mp4

EE = E(K:K+R-1,COL);
FF = F(K:K+R-1,COL);
szEE = size(EE);
blk=permute(reshape(EE',[S S r]),[ 2 1 3]);

normx = sqrt(sum(blk.^2,3));

s = -sign(EE(1:S,:));
u1 = EE(1:S,:) - s.*normx ;

%Mask of positions where u1 ==0. Used for the small top of  B scenario
u1zeromask = (u1==0);


%Skip the top block since it is going to be ones
w = EE(S+1:end,:)./repmat(u1,[r-1 1]);
w = [ones(S);w];
%wc = mat2cell(w,repmat(S,[1 N]));
tau = -s.*u1./normx;

% E := Qk*E
TAU_W = repmat(tau,[r 1]).*w;
%TAU_W_E = zeros(szEE(1),N*S,k+r-1);
TAU_W_E = zeros(szEE(1),size(E,2));
TAU_W_F = zeros(szEE(1),size(F,2));

for i = 1:ncols
    
    %j represents the 3rd dimension along which we will sum.
    for j=1:r
        wc = w((j-1)*S+1:j*S,:);
        TAU_W_WTi = TAU_W.*repmat(wc,[r 1]);
        %Multiply to each block column of E and add
        
        %Column wise multiply
        ej = E((K-1)+(j-1)*S+1:(K-1)+j*S,(i-1)*S+1:i*S);
        
        
        %Sum it on the fly
        TAU_W_E(:,(i-1)*S+1:i*S) = TAU_W_E(:,(i-1)*S+1:i*S) + ...
            TAU_W_WTi .* repmat(ej,[r 1]);

        %This is for the case of the fictitous zeroth column, when F
        %doesn't have the same number of colmns as A
        if i <= k+r-1
            fj = F((K-1)+(j-1)*S+1:(K-1)+j*S,(i-1)*S+1:i*S);
            TAU_W_F(:,(i-1)*S+1:i*S) = TAU_W_F(:,(i-1)*S+1:i*S) + ...
                TAU_W_WTi .* repmat(fj,[r 1]);
        end
    end
end
%TAU_W_E_sum = sum(TAU_W_E,3);
if ~isempty(mask)
    E(K:K+R-1,:) = E(K:K+R-1,:) - TAU_W_E.*fullmask(K:K+R-1,:);
    % F := Qk*F
    F(K:K+R-1,:) = F(K:K+R-1,:) - TAU_W_F.*fullmask(K:K+R-1,1:size(F,2));
else
    E(K:K+R-1,:) = E(K:K+R-1,:) - TAU_W_E;
    % F := Qk*F
    F(K:K+R-1,:) = F(K:K+R-1,:) - TAU_W_F;
end
%Reset the NaNs introduced due to already zeroed out elements in A
E(isnan(E))=oldE(isnan(E));
F(isnan(F))=oldF(isnan(F));
end
