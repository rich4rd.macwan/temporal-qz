function [A,B,X]=qzvec_temporal(A,B,epsa,epsb,alfr,alfi,beta,X,varargin)

flip = false;

% FIND EIGENVECTOR5 OF QUASI-TRIANGULAR MATRICES
% USE B FOR INTERMEDIATE STORAGE

%  DO 500 THRU 590 FOR M = N STEP (-1 OR -2) UNTIL 1
sz = size(A);
n=sz(1);
if sz(1)~=sz(2)
    error ('A must be square!');
end
m = n;
while(m>0)
    
    % 500   continue;
    %For all alfi(m) == 0, do this, real vector
    r = alfi(m,:)==0;
    nel=numel(alfr(m,r));
    alfmr = reshape(alfr(m,r),1,1,nel);
    betmr = reshape(beta(m,r),1,1,nel);
    alfmr( abs(alfmr)<epsa(r)) = 0;
    betmr( abs(betmr)<epsb(r)) = 0;
    B(m,m,r) = 1;
    %TODO: Dont forget to save the values back
    Am = A(:,:,r);
    Bm = B(:,:,r);
    epsam = epsa(r);
    epsbm = epsb(r);
    if(m>1)
        l=m-1;
        l1 = l+1;
        k = l-1;
        if (m==3)
            %% For real alfi(m)==0
            if(any(r))
                sl = (betmr.*Am(l,l1,:)-alfmr.*Bm(l,l1,:)).*Bm(l1,m,:);
                
                %2x2 subblock at 2,2 i.e A(l,l-1)=0
                % B(2,3)
                %2x2 Subblock at 2,2
                sb2 = Am(l,l-1,:)==0;
                %For all sb22==0, calculate d and B(2,3)
                d = betmr(sb2).*Am(l,l,sb2) - alfmr(sb2).*Bm(l,l,sb2);
                dz = d==0;
                if any(dz)
                    epsams = epsam(sb2); epsbms = epsbm(sb2);
                    d(dz) = (epsams(dz)+epsbms(dz))./2;
                end
                Bm(l,m,sb2) = -sl(sb2)./d;
                
                %B(1,3)
                sl(sb2) = (betmr(sb2).*Am(k,l,sb2) - alfmr(sb2).*Bm(k,l,sb2)).*Bm(l,m,sb2)+...
                    (betmr(sb2).*Am(k,m,sb2) - alfmr(sb2).*Bm(k,m,sb2)).*Bm(m,m,sb2);
                %                 sl = (betmr.*A(k,l,r) - alfmr.*B(k,l,r)).*B(l,m,r)+...
                %                     (betmr.*A(k,m,r) - alfmr.*B(k,m,r)).*B(m,m,r);
                d = betmr(sb2).*Am(k,k,sb2) - alfmr(sb2).*Bm(k,k,sb2);
                dz = d==0;
                if any(dz)
                    epsams = epsam(sb2); epsbms = epsbm(sb2);
                    d(dz) = (epsams(dz)+epsbms(dz))./2;
                end
                Bm(k,m,sb2) = -sl(sb2)./d;
                
                %END 2x2 subblock at l-1,l-1 i.e A(l,l-1)=0
                
                % 2x2 Subblock not at 2,2 but at 1,1, i.e for all A(l,l-1) ~=0
                
                % B(1,3)
                %Mask for real and not subblock at (2,2)
                rn2 = ~sb2;
                sk = (betmr(rn2).*Am(k,m,rn2) - ...
                    alfmr(rn2).*Bm(k,m,rn2)).*Bm(l1,m,rn2);
                tkk = betmr(rn2).*Am(k,k,rn2) - alfmr(rn2).*Bm(k,k,rn2);
                tkl = betmr(rn2).*Am(k,l,rn2) - alfmr(rn2).*Bm(k,l,rn2);
                tlk = betmr(rn2).*Am(l,k,rn2);
                tll = betmr(rn2).*Am(l,l,rn2) - alfmr(rn2).*Bm(l,l,rn2);
                d = tkk.*tll - tkl.*tlk;
                dz = d==0;
                if any(dz)
                    epsams = epsam(rn2); epsbms = epsbm(rn2);
                    d(dz) = (epsams(dz)+epsbms(dz))./2;
                end
                Bm(l,m,rn2) = (tlk.*sk - tkk.*sl(~sb2))./d;
                
                Bmr = Bm(:,:,rn2);
                slr = sl(:,:,rn2);
                % B(2,3)
                
                flip = abs(tkk) >= abs(tlk);
                if(any(flip))
                    Bmr(k,m,flip) = -(sk(flip) + tkl(flip).*Bmr(l,m,flip))./tkk(flip);
                end
                if (any(~flip))
                    Bmr(k,m,~flip) = -(slr(~flip) + tll(~flip).*Bmr(l,m,~flip))./tlk(~flip);
                end
                Bm(:,:,rn2) = Bmr;
                %Save back changed Bm values for real eigenvalues
                B(:,:,r) = Bm;
                
                %END 2x2 Subblock not at 2,2 but at 1,1, i.e for all A(l,l-1) ~=0
            end
            %% For real alfi(m)~=0
            if(any(~r))
                %For all alfi(m) ~= 0, do this, complex pair of vectors, c==complex
                nel=numel(alfr(m,~r));
                alfmrc = reshape(alfr(m-1,~r),1,1,nel);
                alfmic = reshape(alfi(m-1,~r),1,1,nel);
                betmc = reshape(beta(m-1,~r),1,1,nel);
                mr = m-1;
                mi = m;
                
                B(m-1,mr,~r) = alfmic.*B(m,m,~r)./(betmc.*A(m,m-1,~r));
                B(m-1,mi,~r) =(betmc.*A(m,m,~r)-alfmrc.*B(m,m,~r))./(betmc.*A(m,m-1,~r));
                B(m,mr,~r) = 0.;
                B(m,mi,~r) = -1.;
                l = m-2;
                l1= l+1;
                slr = zeros(size(betmc));
                sli = slr;
                for j = l1:m
                    tr = betmc.*A(l,j,~r) - alfmrc.*B(l,j,~r);
                    ti = -alfmic.*B(l,j,~r);
                    slr = slr + tr.*B(j,mr,~r) - ti.*B(j,mi,~r);
                    sli = sli + tr.*B(j,mi,~r) + ti.*B(j,mr,~r);
                end
                if(l==1)
                    dr = betmc.*A(l,l,~r) - alfmrc.*B(l,l,~r);
                    di = -alfmic.*B(l,l,~r);
                    [B(l,mr,~r),B(l,mi,~r)] = CDIV_temporal(-slr,-sli,dr,di);
                else
                    m=m-1;
                    continue;
                end
                
                %NOT IMPLEMENTED, for m>3,  the following code is only executed
                %when m>3, which in our case is not needed since we are using
                %N=3, for all A(l,l-1)~=0
                % %             k = l-1;
                % %                 skr = 0.;
                % %                 ski = 0.;
                % %                 for  j=l1:m
                % %                     tr = betm.*A(k,j) - almr.*B(k,j);
                % %                     ti = -almi.*B(k,j);
                % %                     skr = skr + tr.*B(j,mr) - ti.*B(j,mi);
                % %                     ski = ski + tr.*B(j,mi) + ti.*B(j,mr);
                % %                 end
                % %                 tkkr = betm.*A(k,k) - almr.*B(k,k);
                % %                 tkki = -almi.*B(k,k);
                % %                 tklr = betm.*A(k,l) - almr.*B(k,l);
                % %                 tkli = -almi.*B(k,l);
                % %                 tlkr = betm.*A(l,k);
                % %                 tlki = 0.;
                % %                 tllr = betm.*A(l,l) - almr.*B(l,l);
                % %                 tlli = -almi.*B(l,l);
                % %                 dr = tkkr.*tllr - tkki.*tlli - tklr.*tlkr;
                % %                 di = tkkr.*tlli + tkki.*tllr - tkli.*tlkr;
                % %
                % %                 if(dr == 0. && di == 0.)
                % %                     dr =(epsa+epsb)./2.;
                % %                 end
                % %                 [~,~,dr,di,B(l,mr),B(l,mi)]=CDIV(tlkr.*skr-tkkr.*slr+tkki.*sli,tlkr.*ski-tkkr.*sli-tkki.*slr,dr,di,B(l,mr),B(l,mi));
                % %                 flip =(abs(tkkr)+abs(tkki)) >= abs(tlkr);
                % %                 if(flip);
                % %                     [~,~,~, ~, B(k,mr), B(k,mi)]=CDIV(-skr-tklr.*B(l,mr)+tkli.*B(l,mi),-ski-tklr.*B(l,mi)-tkli.*B(l,mr),tkkr, tkki, B(k,mr), B(k,mi));
                % %                 end;
                % %                 if(~ flip);
                % %                     [~,~,~, ~, B(k,mr), B(k,mi)]=CDIV(-slr-tllr.*B(l,mr)+tlli.*B(l,mi),-sli-tllr.*B(l,mi)-tlli.*B(l,mr),tlkr, tlki, B(k,mr), B(k,mi));
                % %                 end;
                % %                 l= l-2;
            end
        end
        
        if (m==2)
            
            %l=1, l1=2
            if(any(r))
                sl = (betmr.*A(l,l1,r)-alfmr.*B(l,l1,r)).*B(l1,m,r);
                d = betmr.*A(l,l,r) - alfmr.*B(l,l,r);
                dz = d==0;
                if any(dz)
                    EPS = (epsa(r)+epsb(r))./2;
                    d(dz) = EPS(dz);
                end
                Bm(l,m,:) = -sl./d;
                B(:,:,r) = Bm;
            end
            cmask=alfi(m,:)~=0 & alfi(m-1,:)~=0;
            if(any(cmask))
                nel=numel(alfr(m,cmask));
                alfmrc = reshape(alfr(m-1,cmask),1,1,nel);
                alfmic = reshape(alfi(m-1,cmask),1,1,nel);
                betmc = reshape(beta(m-1,cmask),1,1,nel);
                mr = m-1;
                mi = m;
                
                B(m-1,mr,cmask) = alfmic.*B(m,m,cmask)./(betmc.*A(m,m-1,cmask));
                B(m-1,mi,cmask) =(betmc.*A(m,m,cmask)-alfmrc.*B(m,m,cmask))./(betmc.*A(m,m-1,cmask));
                B(m,mr,cmask) = 0.;
                B(m,mi,cmask) = -1.;
%                 l = m-2;
%                 l1= l+1;
%                 slr = zeros(size(betmc));
%                 sli = slr;
%                 for j = l1:m
%                     tr = betmc.*A(l,j,cmask) - alfmrc.*B(l,j,cmask);
%                     ti = -alfmic.*B(l,j,cmask);
%                     slr = slr + tr.*B(j,mr,cmask) - ti.*B(j,mi,cmask);
%                     sli = sli + tr.*B(j,mi,cmask) + ti.*B(j,mr,cmask);
%                 end
%                 if(l==1)
%                     dr = betmc.*A(l,l,cmask) - alfmrc.*B(l,l,cmask);
%                     di = -alfmic.*B(l,l,cmask);
%                     [B(l,mr,cmask),B(l,mi,cmask)] = CDIV_temporal(-slr,-sli,dr,di);
%                 else
%                     m=m-1;
%                     continue;
%                 end
            end
        end
    end
    m = m-1;
end

% TRANSFORM TO ORIGINAL COORDINATE SYSTEM
m = n;
X = mtimesx(X,B);
% NORMALIZE SO THAT LARGEST COMPONENT = 1.
% POTENTIAL TYPO in 609
m = n;


% 630   continue;

%% For real alfas
for m =n:-1:1
    r = alfi(m,:)==0;
    nr =numel(find(r));
    if(nr>0)
        [~,idx]=max(abs(X(:,m,r)));
        %Extract the real values of blockwise max of the absolute value of
        %X(:,m)
        d = diag(reshape(X(squeeze(idx),m,r),nr,nr));
        %D = repmat(reshape(d,1,1,numel(d)),n,1);
        for  i=1:n;
            X(i,m,r) = squeeze(X(i,m,r))./d;
        end
        
    end
end

%% For complex alfas, keep in mind they are in pairs
for m=n:-1:2
    cmask = alfi(m,:)~=0 & alfi(m-1,:)~=0;
    nnr =numel(find(cmask));
    if(nnr>0)
        [~,idx]=max((X(:,m-1,cmask)).^2 + (X(:,m,cmask)).^2);
        dr = diag(reshape(X(squeeze(idx),m-1,cmask),nnr,nnr));
        %Dr = repmat(reshape(dr,1,1,numel(dr)),n,1);
        di = diag(reshape(X(squeeze(idx),m,cmask),nnr,nnr));
        %Di = repmat(reshape(di,1,1,numel(di)),n,1);
        for i = 1:n
            [X(i,m-1,cmask),X(i,m,cmask)]=CDIV_temporal(X(i,m-1,cmask),X(i,m,cmask),dr,di);
        end
    end
end

end
