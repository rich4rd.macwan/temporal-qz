function SrPDEVplayer(varargin)
%Extracts spatial rppg metrics from a single video
close all;
addpath('../temporal-qz/');
WINDOW_LENGTH_SEC = 10;
STEP_SEC = .2;
useNewAnalysis = true;
WRITEVIDEO = true;
displayWin = 100;
outFolder = '/home/richard/MEGAsync/Private/Studies/Phd/Papers/SrPDE/JMIV/';
method = 2; % SP=0 or SrPDE=1 or CHROM=2
DOGREEN = false; %Use Green trace instead of CHROM
i=1;
db.idx = 2; % CasmeSq=1, FixedData=2, MMSE=3;
doFFTSpatial = true;
roisize=[];
if db.idx==1
    vidFolder = '/mnt/nvmedisk/CasmeSq/traces_500_50_MC/rawvideo/s15/15_0101disgustingteeth.avi/';
    dbFolder = '/mnt/nvmedisk/CasmeSq/rawvideo/';
    tokens = strsplit(vidFolder,'/');
    tokens(cellfun(@isempty,tokens))=[];
    fnm=tokens{end};
    fnm_prefix = ['s' fnm(1:7) '_'];
elseif db.idx==2
    vidFolder = '/mnt/nvmedisk/fixedData20122017/2017_12_20-14_55_23/';
    dbFolder = '/mnt/nvmedisk/fixedData20122017/';
end


while(nargin > i)
    if(strcmp(varargin{i},'vidFolder')==1)
        vidFolder=varargin{i+1};
    end
    if(strcmp(varargin{i},'WINDOW_LENGTH_SEC')==1)
        WINDOW_LENGTH_SEC=varargin{i+1};
    end
    if(strcmp(varargin{i},'STEP_SEC')==1)
        STEP_SEC=varargin{i+1};
    end
    if(strcmp(varargin{i},'useCasmeSq')==1)
        useNewAnalysis=varargin{i+1};
    end
    if(strcmp(varargin{i},'roisize')==1)
        roisize=varargin{i+1};
    end
    if strcmp(varargin{i},'method')==1
        if strcmp(varargin{i+1},'SP')
            method = 0;
        elseif strcmp(varargin{i+1},'SrPDE')
            method = 1;
        elseif strcmp(varargin{i+1},'CHROM')
            method = 2;
        end
    end
    i=i+2;
end


if useNewAnalysis
    Fs = 30; %CasmeSq
    Ts = 1/Fs;
    winLength = pow2(floor(log2(WINDOW_LENGTH_SEC*Fs))); % length of the sliding window for FFT
    STEP_SEC = .5;
    halfWin = (winLength/2);
    
    if method==1
        video = UBFCRPPGVideoReader(vidFolder,'format','avi','name','srpderaw.avi');
    end
    
    %Extract original video path
    if db.idx == 1 %Casme
        tokens = strsplit(vidFolder,'/');
        %Trim empty cells
        tokens(cellfun('isempty',tokens))=[];
        %Extract relevant video path and name
        pathsuffix = [tokens{end-1} ];
        videoFull = UBFCRPPGVideoReader([dbFolder pathsuffix '/' ],'format','avi','name',tokens{end});
    end
    
    % load([vidFolder 'faceroi.mat']);
    if method==1
        %Mean of extracted SrPDE
        load([vidFolder 'pulseTrace']);
        sPulseTraceOrig = data.sPulseTrace;
        pulseTrace = data.pulseTrace;
    end
    if method==2
        if ~isempty(roisize)
            load([vidFolder 'pulseTraceCHROM-' num2str(roisize) 'x' num2str(roisize) '.mat']);
        else
            load([vidFolder 'pulseTraceCHROM']);
        end
        
        if DOGREEN
            sPulseTraceOrig = data.G;
        else
            sPulseTraceOrig = data.sPulseTraceCHROM;
        end
    end
    
    if db.idx == 2 %Fixeddata
        videoFull = UBFCRPPGVideoReader(vidFolder,'format','avi','name','vid.avi');
        %Load groundtruth data
        [gtTrace, gtHR, gtTime ] = loadPPG(vidFolder);
        gtTime = gtTime / 1000;  % in seconds
        Fs_PPG = 1/mean(diff(gtTime));
        HR_PPG = zeros(1,data.nWindows);
        HR_rPPG = zeros(1,data.nWindows);        
    end
    
    
    traceSize =int32( videoFull.FrameRate * videoFull.Duration);
    %traceSize = data.nFrames;
    data.timeTrace = linspace(0,videoFull.Duration,traceSize);
    scale = 1;
    LOW_F = .7; UP_F = 3;
    
    
    %For cropping the scaled images
    scalefactor=videoFull.Height/size(sPulseTraceOrig ,1);
    
    scalefordisplay=1;
    if data.useMotionCompensation
        facerois=scalefordisplay*data.faceroi/scalefactor;
    end
    data.roisize=data.roisize/scalefordisplay;
    if doFFTSpatial
        %Create two subfigures for accomodating spatial FFT display
        hf_main = figure('Renderer', 'opengl', 'Position', [0 0 1920 1040],'Menubar', 'none','Toolbar','none','DockControls','off','Color','white','Resize','off');
        leftPanel =  uipanel('Position',[0 0 .5 1]);
        rightPanel =  uipanel('Position',[0.5 0 .5 1]);
        nRows = 3; nCols = 3;
        parent = leftPanel;
    else
        hf_main=figure('Renderer', 'opengl', 'Position', [10 10 1024 1024],'Menubar', 'none','Toolbar','none','DockControls','off','Color','white','Resize','off');
        nRows = 3; nCols = 3;
        parent =hf_main;
    end
    
    %colorbar;
    %displayWin = 50;
    t = 0;
    
    %Load GT
    % [gtTrace, gtHR, gtTime ] = loadPPG(vidFolder);
    % gtTime = gtTime / 1000;  % in seconds
    % Fs_PPG = 1/mean(diff(gtTime));
    
    winLength = data.winLength;
    stepSize = pow2(floor(log2(STEP_SEC*Fs)));
    
    if WRITEVIDEO
        roisuffix = ['-' num2str(data.roisize(1)) 'x' num2str(data.roisize(2)) '.avi'];
        if DOGREEN
            fnm = [vidFolder 'srpdeMetricsG' roisuffix];
        else
            fnm = [vidFolder 'srpdeMetrics' roisuffix];
        end
        if exist(fnm,'file')
            %Delete file
            delete(fnm);
        end
        videowriter = VideoWriter(fnm,'Motion JPEG AVI');
        videowriter.FrameRate = Fs;
        open(videowriter);
    end
    tbl_header    = "\begin{tabular}{|c|c|c|c|c|} \hline  \textbf{Framerate} &  \textbf{t(s)} & \textbf{Frame} \# & \textbf{Total frames} & \textbf{Window Length}\\ \hline";
    tbl_footer    = "\\ \hline \end{tabular}";
    handles = struct;
    handles.h334 = [];
    winIndex = 1;
    
    %Temporal normalization
    if method == 1 %SrPDE
        %Normalize temporally :max-min
        smax = max(max(max(sPulseTraceOrig)));
        smin = min(min(min(sPulseTraceOrig)));
        sPulseTraceOrig  = (sPulseTraceOrig - smin)/(smax-smin);
        
        LOW_F = .7; UP_F = 3;
        FILTER_ORDER = 8;
        [data.bcoeff,data.acoeff] = butter(FILTER_ORDER,[LOW_F,UP_F ]/(data.Fs/2));
        
        sPulseTraceAvg = squeeze(nanmean(nanmean(sPulseTraceOrig)));
        sPulseTraceAvg = filter(data.bcoeff,data.acoeff,detrendsignal(sPulseTraceAvg));
        mn = min(sPulseTraceAvg);
        mx = max(sPulseTraceAvg);
        sPulseTraceAvg = (sPulseTraceAvg-mn)/(mx-mn);
        
    end
    
    if method == 2 %CHROM
        smax = max(max(max(sPulseTraceOrig)));
        smin = min(min(min(sPulseTraceOrig)));
        sPulseTraceOrig  = (sPulseTraceOrig - smin)/(smax-smin);
        sPulseTraceAvg = nanmean(sPulseTraceOrig,2);
        %         mn = min(sPulseTraceAvg);
        %         mx = max(sPulseTraceAvg);
        %         sPulseTraceAvg = (sPulseTraceAvg-mn)/(mx-mn);
        %pulseTrace = sPulseTraceAvg;
        pulseTrace = filter(data.bcoeff,data.acoeff,detrendsignal(sPulseTraceAvg));
    end
    
    if scalefordisplay~=1
        sPulseTrace = blockproc(sPulseTraceOrig ,[1,1]/scalefordisplay,data.fnroimean);
        sPulseTrace = (sPulseTrace - mn)/(mx-mn);
    else
        %sPulseTrace = blockproc(data.sPulseTrace,[2 2],data.fnroimean);
        %fnroiexpand = @(block_struct) repmat(block_struct.data,2,2);
        
        %sPulseTrace = blockproc(sPulseTrace,[2 2],fnroiexpand);
        sPulseTrace = sPulseTraceOrig ;
    end
    
    %data.pulseTrace = data.pulseTrace - mean(data.pulseTrace);
    
    startInd=1;
    
    %Save rPPG spatial HR if using inhouse database
    if db.idx == 2 && doFFTSpatial
        HR_rPPG_spatial = zeros(size(sPulseTrace,2),data.nWindows);
        HR_rPPG_spatial_avg = zeros(1,data.nWindows);
    end
    %Window index for storing HRs
    winIdx = 0;
    for i=1:traceSize
        frameLarge = readFrameUBFCRPPG(videoFull);
        if i==1
            frameFull = im2double(frameLarge);
            
        end
        %     frameFull = (frameFull(faceroi(1):faceroi(1)+faceroi(3)-1,...
        %                faceroi(2):faceroi(2)+faceroi(4)-1,:));
        %frame = im2double(readFrameUBFCRPPG(video));
        %srpde = data.sPulseTrace(:,:,i);
        %srpde = blockproc(data.sPulseTrace(:,:,i),[1,1]/scalefordisplay,data.fnroimean);
        if method==1
            srpde = sPulseTrace(:,:,i);
        end
        if method==2
            srpde = reshape(sPulseTrace(i,:),data.nVCells,data.nHCells);
        end
        if winIndex==1
            if mod(i,data.winLength)==0
                winIndex = winIndex +1;
            end
        else
            if(mod(i,data.stepSize)==0)
                winIndex = winIndex +1;
            end
        end
        
        
        idx=isnan(srpde);
        
        if i==1
            if data.useMotionCompensation
                %Crop a rect around face such that the face remains stationary
                extents = [data.faceroi(i,2),data.faceroi(i,2)+data.faceroi(i,4)-1, data.faceroi(i,1),data.faceroi(i,1)+data.faceroi(i,3)-1];
                if extents(2)>size(data.frame,1)
                    extents(2) = size(data.frame,1);
                end
                if extents(4) > size(data.frame,2)
                    extents(4) = size(data.frame,2);
                end
                frameFull = frameFull(extents(1):extents(2),extents(3):extents(4),:);
                
                %frameFull = (frameFull(data.faceroi(i,2):data.faceroi(i,2)+data.faceroi(i,4)-1,...
                %data.faceroi(i,1):data.faceroi(i,1)+data.faceroi(i,3)-1,:));
                
            end
            
            %Logic for overlaying srpde with rgb. Lot of trial and error.
            %Doesn't work too well. Better to look at the imagesc of srpde
            rgb=(blockproc(frameFull,data.roisize,data.fnroimean));
            r=rgb(:,:,1); g=rgb(:,:,2); b=rgb(:,:,3);
            r(idx)=nan; g(idx)=nan;b(idx)=nan;
            if method == 1
                rscaled = r.*sPulseTrace.^2;
                gscaled = g.*sPulseTrace.^2;
                bscaled = b.*sPulseTrace.^2;
                maxscaled =max( [max(max(max(rscaled))),max(max(max(gscaled))),max(max(max(bscaled)))]);
                minscaled =min([ min(min(min(rscaled))),min(min(min(gscaled))),min(min(min(bscaled)))]);
                fac = 1/(maxscaled-minscaled);
                rscaled = (rscaled-minscaled)*fac;
                gscaled = (gscaled-minscaled)*fac;
                bscaled = (bscaled-minscaled)*fac;
                rgbscaled = cat(3,rscaled(:,:,i),gscaled(:,:,i),bscaled(:,:,i));
                img = mat2gray(rgbscaled .*srpde);
            end
            if method == 2
                img = mat2gray(rgb.*srpde);
            end
            
            %rgb=cat(3,r,g,b);
            %srpde = sPulseTrace(:,:,i);
            subplot(nRows,nCols,1,'Parent',parent);
            handles.h331=image(frameLarge);
            images.roi.Rectangle(gca,'Position',data.faceroi(i,:));
            axis equal;
            title('Original video','FontSize',18);
            handles.s332=subplot(nRows,nCols,2);
            if data.useMotionCompensation
                handles.h332=image(img);
            else
                handles.h332=image(imcrop(img,facerois(i,:)));
            end
            axis equal;
            
            handles.notnans = ~isnan(sPulseTrace(1,:));
            %handles.cmap=[232 144 5;236 117 5; 216 74 5;244 43 3;231 14 2;6 214 160;199 237 250;7 59 76]./255;
            handles.cmap = spring(numel(find(handles.notnans)));
            %handles.cmap = softmax(rand(3,numel(find(handles.notnans))));
            %Draw annotations if we have srpde of 3x3
            idx = 1;
            if size(srpde,1)<10
                for r = 1:size(srpde,1)
                    for c = 1:size(srpde,2)
                        if ~isnan(srpde(r,c))
                            text(handles.s332,c,r,num2str(idx),'Color','white','BackGroundColor',handles.cmap(idx,:),'FontSize',6);
                            idx = idx + 1;
                        end
                    end
                end
            end
            
            
            %srpde = imresize(srpde,[faceroi(3) faceroi(4)],'box');
            %imshowpair( frameFull,imresize(frame,.03).*mat2gray(srpde),'montage');
            title('SrPDE overlayed on skin','FontSize',18);
            handles.s333 = subplot(nRows,nCols,3,'Parent',parent);
            axis(handles.s333,'off');
            %handles.h333=image(srpde*255);
            handles.h333=imagesc(srpde,[0 1]);
            %colorbar
            daspect([1 1 1]);
            title('SrPDE raw','FontSize',18);
            handles.s334=subplot(nRows,nCols,[4 5 6],'Parent',parent);
            rotate3d(handles.s334,'on');
            set(handles.s334,'FontSize',18);
            grid on;
            hold on;
            %t=title(['Framerate = ' num2str(Fs) '          Frame# ' num2str(i) '           t = ' num2str(t) 's']);
            tbl_body = sprintf("%d & %d & %04d & %d & %d (s), %d frames", Fs, t,i, traceSize,WINDOW_LENGTH_SEC,winLength);
            tbl_str = sprintf("%s %s %s", tbl_header, tbl_body, tbl_footer);
            handles.t334=title({tbl_str }, 'Interpreter','latex','FontSize',18);
            
            handles.s325 = subplot(nRows,nCols-1,5,'Parent',parent);
            xlim([0 3.5]);
            ylim auto;
            grid on;
            hold on;
            title('PPG FFT','FontSize',18);
            %text(0.5,60,sprintf("Accumulating %d frames \nequivalent to %ds",winLength,WINDOW_LENGTH_SEC));
            if db.idx == 1
                handles.t325=text(0.5,60,sprintf("No PPG GT for CasmeSq db"));
            end
            
            handles.s326 = subplot(nRows,nCols-1,6,'Parent',parent);
            title('rPPG FFT','FontSize',18);
            grid on;
            hold on;
            %handles.t326=text(0.5,60,sprintf("Accumulating %d frames \nequivalent to %ds",winLength,WINDOW_LENGTH_SEC));
            xlim([0 3.5]);
            ylim auto;
            axis(handles.s333,'off');
            drawnow;
            
        else
            if(mod(i-1,stepSize)==0)
                doFFT = true;
                winIdx = winIdx + 1;
            else
                doFFT = false;
            end
            if method==1
                img = mat2gray(rgbscaled.*srpde);
                
            end
            
            if method == 2
                img = mat2gray(rgb.*srpde);
            end
            
            if doFFT
                if i+winLength - 1 > data.nFrames
                    pulseTraceWin = pulseTrace(i:data.nFrames);
                    sPulseTraceWin = sPulseTrace(i:data.nFrames,:)';
                else
                    pulseTraceWin = pulseTrace(i:i+winLength-1);
                    sPulseTraceWin = sPulseTrace(i:i+winLength-1,:)';
                end
                
                if db.idx==2 %Ground truth available
                    if i+winLength-1>numel(gtTime)
                        gtTimeWin = gtTime(i:numel(gtTime));
                        gtTraceWin = gtTrace(i:numel(gtTime));
                    else
                        gtTimeWin = gtTime(i:i+winLength-1);
                        gtTraceWin = gtTrace(i:i+winLength-1);
                    end
                    
                    %GT with noplot set to true
                    [~, ~, PPG_peaksLoc,handles] = fftPPG(gtTimeWin,gtTraceWin, Fs_PPG,'noplot',false,'handles',handles);
                    if ~isempty(PPG_peaksLoc)
                        HR_PPG(winIdx) = round(PPG_peaksLoc(1)*60);
                    end
                end
                
                if i<data.nFrames-stepSize
                    
                    [~, ~, rPPG_peaksLoc, handles] = fftrPPG(pulseTraceWin,Fs,'noplot',false,'handles',handles);
                    if ~isempty(rPPG_peaksLoc)
                        HR_rPPG(winIdx) = round(rPPG_peaksLoc(1)*60);
                    end
                    if doFFTSpatial
                        if ~isfield(handles,'spanel2')
                            handles.spanel2=axes('Parent',rightPanel);
                            rotate3d(handles.spanel2,'on');
                            title('rPPG FFT Spatial');
                            grid on;
                            hold on;
                        end
                        [~, ~, rPPG_peaksLoc, handles] = fftrPPG(sPulseTraceWin,Fs,'noplot',false,'handles',handles,'meanTrace',pulseTraceWin);
                        HR_rPPG_spatial(:,winIdx) = rPPG_peaksLoc(:,1)*60;
                        HR_rPPG_spatial_avg(winIdx) = mean(HR_rPPG_spatial(:,winIdx));
                    end
                    
                end
                drawnow;
            end
            
            if method==2
                DOFILTER = false;
                %For CHROM, filter the srpde around heart rate
                if DOFILTER && i>displayWin && data.nFrames-i>displayWin/2
                    HR = rPPG_peaksLoc(1);
                    FILTER_ORDER = 8;
                    [bcoeff,acoeff] = butter(FILTER_ORDER,[HR-.05,HR+.05 ]/(data.Fs/2));
                    srpdeWin = filter(data.bcoeff,data.acoeff,sPulseTraceOrig(i-displayWin/2:i+displayWin/2,:));
                    srpdeWin = (srpdeWin-smax)./(smax-smin);
                    srpde = reshape(srpdeWin(displayWin/2,:),data.nVCells,data.nHCells);
                    srpdeAvg = nanmean(srpdeWin,2);
                    %Update pulseTrace to reflect filtered srpde
                    %pulseTrace(i-displayWin/2:i+displayWin/2) = srpdeAvg;
                    %hold(handles.s334,'on');
                    %handles.srpdeAvg = plot(handles.s334,data.timeTrace(startInd:i),srpdeAvg(1:i-startInd+1));
                end
            end
            
            %             frameFull = (frameLarge(data.faceroi(i,2):data.faceroi(i,2)+data.faceroi(i,4)-1,...
            %             data.faceroi(i,1):data.faceroi(i,1)+data.faceroi(i,3)-1,:));
            %
            %             img = mat2gray(blockproc(frameFull,data.roisize,data.fnroimean));
            set(handles.h331,'CData',frameLarge);
            set(handles.h332,'CData',img);
            set(handles.h333,'CData',srpde);
            set(handles.s333,'CLim',[0,1]);
            
            
            
            
            if i>displayWin/2
                %startInd=data.winLength + i-displayWin/2;
                startInd = i-displayWin/2;
            end
            
            if(i==2)
                if method==2
                    Z = sPulseTrace(startInd:startInd+i-1,handles.notnans);
                    handles.Y =repmat(1:size(Z,2),size(Z,1),1);
                    handles.nns = numel(find(handles.notnans));
                    %The pixelwise spulsetrace
                    if size(srpde,1)<10
                        handles.srpde = plot3(handles.s334,data.timeTrace(startInd:startInd+i-1),handles.Y,Z,'MarkerSize',6);
                        handles.wavefrontcolor = [.2,.4,.8];
                        handles.wavefront = plot3(handles.s334, repmat(data.timeTrace(i),handles.nns,1),1:handles.nns,sPulseTrace(i,handles.notnans),'-o','Color',handles.wavefrontcolor);
                        set(handles.wavefront,'MarkerFaceColor',handles.wavefrontcolor);
                    end
                    view(handles.s334,[0 0]);
                    rotate3d(handles.s334,'on');
                    handles.h334 = plot3(handles.s334,data.timeTrace(startInd:startInd+i-1),zeros(1,size(Z,1)),sPulseTraceAvg(startInd:startInd+i-1),'-o','MarkerSize',6,'Color',[.3 .5 .9],'LineWidth',2);
                    set(handles.s334,'ColorOrder',handles.cmap);
                    set(handles.s334,'Xlim',[data.timeTrace(startInd), data.timeTrace(startInd+displayWin)])
                    set(handles.s334,'YTick',0:2:numel(find(handles.notnans)));
                end
                %hold on;
                %handles.h334=plot(handles.s334,data.timeTrace(startInd:i),sPulseTraceWin,'-o','MarkerSize',6,'Color',[.9 .5 .3],'LineWidth',2);
                %set(handles.s334,'Ylim',[0 1]);
            else
                %For the initial few frames, until the waveform reaches the
                %center of the plot
                if i<displayWin/2
                    
                    if method==2
                        Z = sPulseTrace(startInd:startInd+i-1,handles.notnans);
                        handles.Y = repmat(1:size(Z,2),size(Z,1),1);
                        
                        % sPulseTraceWin = sPulseTraceAvg(startInd:i);
                        set(handles.h334,'XData',data.timeTrace(startInd:startInd + i-1),'YData',zeros(1,size(Z,1)),'ZData',sPulseTraceAvg(startInd:startInd+i-1));
                        if size(srpde,1)<10
                            idx = 1;
                            for n = 1:size(sPulseTrace,2)
                                if handles.notnans(n)==1
                                    set(handles.srpde(idx),'XData',data.timeTrace(startInd:startInd + i-1),'YData',handles.Y(:,idx),'ZData',sPulseTrace(startInd:startInd+i-1,n));
                                    idx = idx + 1;
                                end
                            end
                            set(handles.wavefront,'XData', repmat(data.timeTrace(i),handles.nns,1),'YData',1:handles.nns,'ZData',sPulseTrace(i,handles.notnans));
                        end
                    end
                    
                    if startInd+displayWin < data.nFrames
                        set(handles.s334,'Xlim',[data.timeTrace(startInd), data.timeTrace(startInd+displayWin)]);
                    else
                        set(handles.s334,'Xlim',[data.timeTrace(startInd), data.timeTrace(data.nFrames)]);
                    end
                else
                    %We've reached the center of the plot, scroll the
                    %waveform
                    if method==2
                        % sPulseTraceWin = sPulseTraceAvg(startInd:i);
                        Z = sPulseTrace(startInd:i,handles.notnans);
                        handles.Y = repmat(1:size(Z,2),size(Z,1),1);
                        
                        %set(handles.h334,'XData',data.timeTrace(startInd:i),'YData',pulseTrace(startInd:i));
                        set(handles.h334,'XData',data.timeTrace(startInd:i),'YData',zeros(1,size(Z,1)),'ZData',sPulseTraceAvg(startInd:i));
                        if size(srpde,1)<10
                            idx = 1;
                            for n = 1:size(sPulseTrace,2)
                                if handles.notnans(n)==1
                                    set(handles.srpde(idx),'XData',data.timeTrace(startInd:i),'YData',handles.Y(:,idx),'ZData',sPulseTrace(startInd:i,n));
                                    idx = idx + 1;
                                end
                            end
                            set(handles.wavefront,'XData', repmat(data.timeTrace(i),handles.nns,1),'YData',1:handles.nns,'ZData',sPulseTrace(i,handles.notnans));
                        end
                    end
                    if startInd+displayWin < data.nFrames
                        set(handles.s334,'Xlim',[data.timeTrace(startInd), data.timeTrace(startInd+displayWin)]);
                    else
                        set(handles.s334,'Xlim',[data.timeTrace(startInd), data.timeTrace(data.nFrames)]);
                    end
                end
                
            end
            tbl_body = sprintf("%d & %d & %04d & %d & %d (s), %d frames", Fs, t,i, traceSize,WINDOW_LENGTH_SEC,winLength);
            tbl_str = sprintf("%s %s %s", tbl_header, tbl_body, tbl_footer);
            set(handles.t334,'String',{tbl_str });
            drawnow;
            
            %% For saving images for the article
            %           if i>displayWin && i>530 && i<585
            %             figure;
            %             hold on;
            %             f=plot(data.timeTrace(i-displayWin:i+displayWin),data.pulseTrace(i-displayWin:i+displayWin)...
            %             ,'-o','MarkerSize',6,'Color',[.3, .5, .9],'LineWidth',3);
            %             set(f,'MarkerFaceColor',[.3 .3 .3]);
            %             grid on;
            
            %             p=plot(data.timeTrace(pts),data.pulseTrace(pts),'o','MarkerSize',12,'Color',[.3, .5, .9],'LineWidth',3);
            %             set(p,'MarkerFaceColor',[.3 .3 .3]);
            %              figure(3);
            %              iptsetpref('ImshowBorder','tight');
            %              imagesc(imresize(imcrop(srpde,facerois(i,:)),40,'box'));
            %              axis equal; axis off;
            %imwrite(imresize(imcrop(img,facerois(i,:)),40,'box'),[outFolder fnm_prefix  num2str(i) '.png']);
            %saveas(gcf,[outFolder fnm_prefix  num2str(i) '.png']);
            
            %exportgraphics(handles.s333,[outFolder fnm_prefix  num2str(i) '.png'])
            %             end
            
            %FFTs after winLength and every stepSize
            
        end
        
        %         if(i>displayWin && i<traceSize - displayWin)
        %             set(handles.s334,'XLim',[data.timeTrace(i-displayWin) data.timeTrace(i+displayWin)]);
        %         end
        if(data.timeTrace(i) > t+1)
            t = int32(data.timeTrace(i));
        end
        if WRITEVIDEO
            %pause(.0001);
            writeVideo(videowriter,getframe(gcf));
        end
    end
    if WRITEVIDEO
        close(videowriter);
    end
    
    %Clean up HR structures
    strayvals = HR_PPG==0;
    HR_PPG(strayvals) = [];
    HR_rPPG(strayvals) = [];
    HR_rPPG_spatial(:,strayvals) = [];
    HR_rPPG_spatial_avg(strayvals) = [];
    if DOGREEN
        save([vidFolder 'HRmetricsG-' num2str(data.roisize(1)) 'x' num2str(data.roisize(2)) '.mat'],'HR_PPG','HR_rPPG','HR_rPPG_spatial','HR_rPPG_spatial_avg');
    else
        save([vidFolder 'HRmetrics-' num2str(data.roisize(1)) 'x' num2str(data.roisize(2)) '.mat'],'HR_PPG','HR_rPPG','HR_rPPG_spatial','HR_rPPG_spatial_avg');
    end
else
    %% For inhouse database, deprecated
    
    video = UBFCRPPGVideoReader(vidFolder,'format','avi','name','rppgOut.avi');
    videoFull = UBFCRPPGVideoReader(vidFolder,'format','avi','name','vid.avi');
    load([vidFolder 'faceroi.mat']);
    
    Fs = video.FrameRate;
    Ts = 1/Fs;
    winLength = pow2(floor(log2(WINDOW_LENGTH_SEC*Fs))); % length of the sliding window for FFT
    STEP_SEC = .5;
    halfWin = (winLength/2);
    traceSize =int32( video.FrameRate * video.Duration);
    timeTrace = linspace(0,video.Duration,traceSize);
    scale = 1;
    LOW_F = .7; UP_F = 3;
    %Mean of extracted SrPDE
    load([vidFolder 'pulseTrace']);
    figure('Renderer', 'zbuffer', 'Position', [10 10 1024 1024],'Menubar', 'none','Toolbar','none','DockControls','off','Color','white','Resize','off');
    subplot(2,2,[3 4]);
    grid on;
    subplot(2,2,2);
    colorbar;
    t = 0;
    
    %Load GT
    [gtTrace, gtHR, gtTime ] = loadPPG(vidFolder);
    gtTime = gtTime / 1000;  % in seconds
    Fs_PPG = 1/mean(diff(gtTime));
    
    winLength = pow2(floor(log2(WINDOW_LENGTH_SEC*Fs))); % length of the sliding window for FFT
    halfWin = (winLength/2);
    stepSize = pow2(floor(log2(STEP_SEC*Fs)));
    
    
    % videowriter = VideoWriter([vidFolder 'srpdeMetrics.mp4'],'MPEG-4');
    % videowriter.FrameRate = Fs;
    % videowriter.Quality=100;
    % open(videowriter);
    
    for i=1:traceSize
        frameFull = readFrameUBFCRPPG(videoFull);
        frameFull = (frameFull(faceroi(1):faceroi(1)+faceroi(3)-1,...
            faceroi(2):faceroi(2)+faceroi(4)-1,:));
        frame = im2double(readFrameUBFCRPPG(video));
        srpde = sPulseTrace(:,:,i).*SNRpx_all;
        %srpde = sPulseTrace(:,:,i);
        subplot(3,3,1);
        imshow(frameFull);
        title('Original video','FontSize',18);
        subplot(3,3,2);
        imshow(imresize(frame,.03).*mat2gray(srpde));
        %srpde = imresize(srpde,[faceroi(3) faceroi(4)],'box');
        %imshowpair( frameFull,imresize(frame,.03).*mat2gray(srpde),'montage');
        title('SrPDE overlayed on skin','FontSize',18);
        subplot(3,3,3);
        %imagesc(srpde);
        imagesc(srpde);
        
        daspect([1 1 1]);
        title('SrPDE raw','FontSize',18);
        subplot(3,3,[4 5 6]);
        grid on;
        hold on;
        if(i>1)
            plot(timeTrace(i-1:i),pulseTrace(i-1:i),'-o','MarkerSize',6,'Color',[.3 .5 .9],'LineWidth',2);
        end
        if(i>displayWin && i<traceSize - displayWin)
            xlim([timeTrace(i-displayWin) timeTrace(i+displayWin)]);
        end
        if(timeTrace(i) > t+1)
            t = int32(timeTrace(i));
        end
        tbl_header    = "\begin{tabular}{|c|c|c|c|c|} \hline  \textbf{Framerate} &  \textbf{t(s)} & \textbf{Frame} \# & \textbf{Total frames} & \textbf{Window Length}\\ \hline";
        tbl_footer    = "\\ \hline \end{tabular}";
        %t=title(['Framerate = ' num2str(Fs) '          Frame# ' num2str(i) '           t = ' num2str(t) 's']);
        tbl_body = sprintf("%.2f & %d & %d & %d & %d (s), %d frames", Fs, t,i, traceSize,WINDOW_LENGTH_SEC,winLength);
        tbl_str = sprintf("%s %s %s", tbl_header, tbl_body, tbl_footer);
        T=title(...
            {...
            tbl_str ...
            }, ...
            'Interpreter','latex' ...
            ,'FontSize',18);
        %FFTs after winLength and every stepSize
        if(i>winLength && mod(i-winLength-1,stepSize)==0)
            gtTimeWin = gtTime(i-winLength:i-1);
            gtTraceWin = gtTrace(i-winLength:i-1);
            pulseTraceWin = pulseTrace(i-winLength:i-1);
            
            subplot(3,2,5);
            %GT with noplot set to true
            [PPGlag, PPG_peaksLoc, PPG_freq,PPG_power] = fftPPG(gtTimeWin,gtTraceWin, Fs_PPG,false);
            title('PPG FFT','FontSize',18);
            ylim auto
            subplot(3,2,6);
            [rPPG_freq, rPPG_powers] = fftrPPG(pulseTraceWin,Fs,-1,false);
            title('rPPG FFT','FontSize',18);
            ylim auto
            pause(.01);
        else
            if(i<=winLength)
                subplot(3,2,5);
                xlim([0 3.5]);
                ylim([0 100]);
                grid on
                title('PPG FFT','FontSize',18);
                cla;
                text(0.5,60,sprintf("Accumulating %d frames \nequivalent to %ds",winLength,WINDOW_LENGTH_SEC));
                
                subplot(3,2,6);
                title('rPPG FFT','FontSize',18);
                grid on
                cla;
                text(0.5,60,sprintf("Accumulating %d frames \nequivalent to %ds",winLength,WINDOW_LENGTH_SEC));
                xlim([0 3.5]);
                ylim([0 100]);
                pause(.01);
            end
            
        end
        %     writeVideo(videowriter,getframe(gcf));
    end
    % close(videowriter);
end

end