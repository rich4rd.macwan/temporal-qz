function [E,F] = householder_r_temporal(E,F,r,k,col)
%%Find householder reflections to eliminate elements k+1:k+r-1 of column
%%col
%%NxN = block size and 1xS*S is the number of blocks
%%Total size of matrix is NxN*S*S
%%E is interlaced and folded to a NxNxS*S tensor


EE = squeeze(E(k:k+r-1,col,:));
normx = sqrt(sum(EE.*EE));
normx = reshape(normx,1,1,size(E,3));

s = -sign(E(k,col,:));
u1 = E(k,col,:) - s.*normx;

w = [ones(1,1,size(E,3));E(k+1:k+r-1,col,:)./repmat(u1,numel(k+1:k+r-1),1)];

%w(1) = 1;
wt = mtimesx(w,'T',1);
w_wt = mtimesx(w,wt);
tau = -s.*u1./normx;
tau_w_wt=repmat(tau,size(w_wt,1),size(w_wt,1)).*w_wt;
%TODO for TAU_W_E flatten E in the third dim and repeat in the 1st
%or use mtimesx
TAU_W_E = mtimesx(tau_w_wt,E(k:k+r-1,:,:));
TAU_W_F = mtimesx(tau_w_wt,F(k:k+r-1,:,:));
% E := Qk*E
E(k:k+r-1,:,:) = E(k:k+r-1,:,:)-TAU_W_E;
% F := Qk*F
F(k:k+r-1,:,:) = F(k:k+r-1,:,:)-TAU_W_F;
end
