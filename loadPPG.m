function [gtTrace, gtHR, gtTime] = loadPPG(vidFolder,varargin)
% load PPG recorded with pulse oximeter

gtHR = [];
gtTrace = [];
gtTime = [];
i=1;
useNewDB = false;
database = 'NONE';
gtoriginal = false;
while(nargin > i)
    if(strcmp(varargin{i},'database')==1)
        database=varargin{i+1};
    end
    if(strcmp(varargin{i},'useNewDB')==1)
        useNewDB=varargin{i+1};
    end
    if(strcmp(varargin{i},'gtoriginal')==1)
        gtoriginal=varargin{i+1};
    end
    i=i+1;
end

% Updated database on the NAS, with refined traces
if useNewDB
    if exist([ vidFolder '/ground_truth.txt' ] )
        % load ground truth
        ground_truth = dlmread( [ vidFolder '/ground_truth.txt' ] );

        gtTrace = ground_truth( 1, : );
        gtHR = ground_truth( 2, : );
        gtTime = ground_truth( 3, : );
    else
        error('loadPPG => oops, no PPG file, or path not accessible...\n');
    end

else
    
    if strcmp(database,'MMSE')
        tokens=strsplit(vidFolder,'/');
        if strcmp(tokens{end},'')==1
            %Last element is a blank
            taskid=tokens{end-1};
            subjectid=tokens{end-2};
            subdb=tokens{end-3};
            prefix=strjoin(tokens(1:end-4),'/');
        else
            taskid=tokens{end};
            subjectid=tokens{end-1};
            subdb=tokens{end-2};
            prefix=strjoin(tokens{1:2},'/');
        end
        if strcmp(subdb,'first 10 subjects 2D')==1
            physdata_path=[prefix ...
                '/first 10 subjects Phydata released/Phydata/' ...
                subjectid '/' taskid '/'];
        elseif strcmp(subdb,'T10_T11_30Subjects')==1
            physdata_path=[prefix ...
                '/T10_T11_30PhyBPHRData/' ...
                subjectid '/' taskid '/'];
        end
        physdata_filename=[physdata_path 'Pulse Rate_BPM.txt'];
        %Load physdata_filename in gtHR, 1000 frames/sec
        fileID = fopen(physdata_filename,'r');
        gtHR = fscanf(fileID,'%f');
        fclose(fileID);
        gtTime = 0:numel(gtHR)-1;
%         gtTime = gtTime(:)/1000;
        gtHR = gtHR(:);
    else
        if gtoriginal
            gtfilename=[vidFolder(1:end-6) '/gtoriginal/' 'gtdump.xmp'];
        else
            gtfilename=[vidFolder '/' 'gtdump.xmp'];
        end
        if exist(gtfilename)==2
            gtdata=csvread(gtfilename);
            gtTrace=gtdata(:,4);
            gtTime=gtdata(:,1);
            gtHR = gtdata(:,2);

            % normalize data (zero mean and unit variance)
            gtTrace = gtTrace - mean(gtTrace,1);
            gtTrace = gtTrace / std(gtTrace);

        else
            error('Loading xmp file: oops, no PPG file...\n');
        end
        
    end
end