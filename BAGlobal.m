function BAGlobal(varargin)
close all;
i=1;
dbase = 'CASMESQ';
method = 'SRPDE';
tracesFolder = '/mnt/nvmedisk/CasmeSq/traces_500_50/rawvideo/';
while(nargin > i)
    if(strcmp(varargin{i},'dbase')==1)
        dbase=varargin{i+1};
    end
    if(strcmp(varargin{i},'method')==1)
        method=varargin{i+1};
    end
    i=i+2;
end

if strcmp('dbase','CASMESQ')==1
    tracesFolder = '/mnt/nvmedisk/CasmeSq/traces_500_50/rawvideo/';
end
%% Access the CasmeSq directory structure
dirs = dir(tracesFolder);
vididx = 1;
badata = struct;
%Iterate through each dir. Start from 3 to skip . and .. listing
if ~exist([tracesFolder '../SrPDEBA.mat'],'file')
    for didx=3:numel(dirs)
        %We have subject directories here: s15, s16,etc.
        viddir = dir([dirs(didx).folder '/' dirs(didx).name '/*.avi']);
        %We have the video directories here, we can access the .seg files now
        
        %Find all the expressions for this subject. Each subject has multiple
        %videos
        sid = str2num(dirs(didx).name(2:end));
        %all_expr_gt = tcodefinal(tnaming_rule1{:,1}==sid,:);
        
        for vidx=1:numel(viddir)
            %Load pulseTrace data
            %fprintf('Loading %s%s\n',viddir(vidx).folder,viddir(vidx).name);
            fnm = [viddir(vidx).folder '/' viddir(vidx).name '/pulseTrace.mat'];
            if exist(fnm,'file')
                %Load pulseTrace
                load(fnm);
                badata(vididx).name = viddir(vidx).name;
                badata(vididx).code = viddir(vidx).name(4:7);
                badata(vididx).codename= viddir(vidx).name(8:end-4);
                badata(vididx).HR_rPPG_avg = data.HR_rPPG_avg;
                badata(vididx).HR_rPPG_win = reshape(data.HR_rPPG_win,...
                    size(data.HR_rPPG_win,1)*size(data.HR_rPPG_win,2),[]);
                badata(vididx).HR_rPPG_win (badata(vididx).HR_rPPG_win ==0)=nan;
                %Each bxbxwinsize rPPG reshaped to b*bxwinSize, columnwise and
                %concatenated
                vididx = vididx+1;
                
            end
            
        end
    end
    save([tracesFolder '../SrPDEBA'],'badata');
end

load([tracesFolder '../SrPDEBA']);
ind = 1;
hold on;
allHRs = cat(2,badata(:).HR_rPPG_win);
allHRs_avg = nanmean(allHRs);

%HRs = zeros(size(badata(1).HR_rPPG_win,1),numel(badata));
vidMeanHR_rPPG = zeros(1,size(allHRs,2));
vidMeanHR_PPG = zeros(1,size(allHRs,2));
allMAEs = zeros(numel(badata));
vidIdx = 1;
for i = 1:numel(badata)
    %HRs(:,i) = nanmean(badata(i).HR_rPPG_win);
    winSize = numel(badata(i).HR_rPPG_avg);
    
    vidMeanHR_rPPG(i:i+winSize-1) = allHRs_avg(i:i+winSize-1);
    vidMeanHR_PPG(i:i+winSize-1) = badata(i).HR_rPPG_avg;    
    s=scatter(smooth(vidMeanHR_rPPG(i:i+winSize-1)),...
        vidMeanHR_PPG(i:i+winSize-1),150,'Filled',...
        'MarkerEdgeAlpha',1,'MarkerFaceAlpha',.4,'LineWidth',2);
    set(gca,'FontSize',32);
  hold on;
%     s1=scatter(smooth(vidMeanHR_rPPG(ind:ind+winSize-1)),...
%         vidMeanHR_PPG_avg(ind:ind+winSize-1),80,'Filled',...
%         'MarkerEdgeAlpha',1,'MarkerFaceAlpha',.8,'LineWidth',1);
    
    hold on;
    axis equal;
    grid on
    xlim([50 130]);
    ylim([50 130])
    
    ind = ind + winSize ;
    vidIdx = vidIdx + 1;
end
vidMAEs = vidMeanHR_rPPG - vidMeanHR_PPG;
title('Correlation plot');
xlabel('HR_{rPPG} (bpm)');
ylabel('HR_{PPG} (bpm)');
%% Linear regression
hold on;
X=[ones(size(vidMeanHR_rPPG(:))) vidMeanHR_rPPG(:) ];
b=regress(vidMeanHR_PPG(:),X);
x1fit = 50:(max(vidMeanHR_rPPG)-min(vidMeanHR_rPPG))/100:130;
yfit = b(1) + b(2)*x1fit;
% y = b(2)x + b(1)
% r =
r = corrcoef(vidMeanHR_PPG,vidMeanHR_rPPG); r = r(1,2);
linecolor = [ .185 .48 .735 ];
plot(x1fit,yfit,'--','LineWidth',4,'Color',linecolor);
plot(x1fit,x1fit,'LineWidth',4,'Color',linecolor);
set(gca,'FontSize',24);
legend(vids);
%% Bland Altman
diffs = vidMeanHR_PPG - vidMeanHR_rPPG;
means = (vidMeanHR_PPG + vidMeanHR_rPPG)/2;
subplot(1,2,2);

ind = 1;
for i=1:numel(winSizes)
    winSize = winSizes(i);
    scatter(means(ind:ind+winSize-1),diffs(ind:ind+winSize-1),150,...
        'Filled','MarkerEdgeAlpha',.9,'MarkerFaceAlpha',.4,'LineWidth',.75);
    set(gca,'FontSize',32);
    xlim([50 100]);
%     ylim([-20 20]);
    grid on; hold on;
    ind = ind + winSize ;
end
XLIM = xlim;
x = linspace(XLIM(1),XLIM(2),100);
meandiff = mean(diffs);
stddiff = std(diffs);
meanline =[x; ones(size(x))*meandiff];
sdline1 = [x; meandiff + ones(size(x))*stddiff*1.96];
sdline2 = [x; meandiff - ones(size(x))*stddiff*1.96];
plot(meanline(1,:),meanline(2,:),'LineWidth', 3','Color',linecolor);
set(gca,'FontSize',24);
%Right aligned text
mytext(90, meandiff+5,'MEAN',linecolor);
mytext(90, meandiff-1,num2str(meandiff),linecolor);
sdcolor = [.8 .247 .041];
p = plot(sdline1(1,:),sdline1(2,:),'--','LineWidth',3,'Color',sdcolor);
mytext(90,meandiff + stddiff*1.96+5,'+1.96SD',sdcolor);
mytext(90,meandiff + stddiff*1.96-1,num2str(meandiff + stddiff*1.96),sdcolor);

plot(sdline2(1,:),sdline2(2,:),'--','LineWidth',3,'Color',sdcolor);
mytext(90,meandiff - stddiff*1.96+5,'-1.96SD',sdcolor);
mytext(90,meandiff - stddiff*1.96-1,num2str(meandiff - stddiff*1.96),sdcolor);
title('Bland Altman plot');
xlabel('(HR_{PPG}+HR_{rPPG})/2');
ylabel('HR_{PPG}-HR_{rPPG}');
% gnames = {'SrPDE HR analysis'}; % names of groups in data {dimension 1 and 2}
% corrinfo = {'n','r','eq'}; % stats to display of correlation scatter plot
% BAinfo = {'RPC(%)'}; % stats to display onavg Bland-ALtman plot
% limits = [40 160]; % how to set the axes limits
% 
% symbols = 'Num'; % symbols for the data sets (default)
% colors = [.1 .5 .7]; % colors for the data sets
% tit='';
% label = {'HR from RPPG', 'HR from PPG'};
% onlycorr = 0;
% figure;
% [cr, fig, statsStruct] = BlandAltman(vidMeanHR_rPPG,vidMeanHR_PPG,label,tit,gnames,corrinfo,BAinfo,limits,colors,symbols,onlycorr);
end



function mytext(x,y,str,clr)
    XLIM = xlim;
    t=text(x,y,str,'FontSize',28,'Color',clr);
    %Right align
    E=get(t,'Extent'); E(1) =XLIM(end) - E(3)-1;
    set(t, 'Position',[E(1) E(2)]);
end