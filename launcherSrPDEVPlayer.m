function launcherSrPDEVPlayer(overwrite, interpolateVideos,useCasmeSq)
%LAUNCHERSRPDEV Run SrPDEV on all fixedData20122017
% interpolateVideos requires srpdeMetrics.mp4 to be present and uses
% butterdflow to create videos with higher framerates for better
% visualization

%Execute starting from startVid to endVid
startVid =  '';
endVid = '31_0503unnyfarting.avi';
vidStartIdx = 0;
roisize=32; %EStimate srpde metrics for this roisize
if useCasmeSq
    vididx = 1;
    casmeFolder = '/mnt/nvmedisk/CasmeSq/rawvideo/';
    tracesFolder = '/mnt/nvmedisk/CasmeSq/traces_500_50_MC/';
    
    %% Access the CasmeSq directory structure
    dirs = dir([tracesFolder 'rawvideo']);
    %Iterate through each dir. Start from 3 to skip . and .. listing
    for didx=3:numel(dirs)
        %We have subject directories here: s15, s16,etc.
        viddir = dir([dirs(didx).folder '/' dirs(didx).name '/*.avi']);
        for vidx=1:numel(viddir)
            vidFolder = [viddir(vidx).folder '/' viddir(vidx).name '/'];
            
            if ~isempty(startVid) && strcmp(startVid,viddir(vidx).name)~=1
                vididx = vididx + 1;
                if vidStartIdx==0
                    continue;
                end
            else
                vidStartIdx = vididx;
            end
            
            fprintf(vidFolder);
            fprintf('\n');
            SrPDEVplayer('vidFolder',vidFolder,'useCasmeSq',useCasmeSq);
            
            if ~isempty(endVid) && strcmp(endVid,viddir(vidx).name)==1
                return;
            end
            
            vididx = vididx + 1;
            %end
            
        end
    end
    
else
    db = struct;
    db.idx = 2;
    db = getNextVid(db);
    %Flag to process videos from db.vidToProcess till the end
    db.processUntilEnd = true; 
    while db.res
        disp(db.tracesFolder);
        SrPDEVplayer('vidFolder',db.tracesFolder,'roisize',roisize);
        db = getNextVid(db);
    end
%    fixedDataFolder = '/mnt/nvmedisk/fixedData20122017/';
%    folders = dir(fixedDataFolder);
    %ignoreList = {'2017_12_20-14_55_23', '2017_12_20-15_02_30','2017_12_20-15_06_34'};
%     for i=1:size(folders,1)
%         skip = false;
%         if ~strcmp(folders(i).name,'.') && ~strcmp(folders(i).name,'..')
%             if(~interpolateVideos)
%                 srpdeOut = dir([fixedDataFolder folders(i).name '\srpdeMetrics.mp4']);
%                 if ~isempty(srpdeOut) && srpdeOut.bytes ~=0
%                     fprintf('%s already processed, skipping... \n',folders(i).name);
%                     skip = true;
%                 end
%                 if(overwrite || ~skip)
%                     d = folders(i);
%                     fprintf('SrPDEV on %s%s\\ \n',fixedDataFolder,d.name);
%                     SrPDEVplayer('vidFolder', [fixedDataFolder d.name '/'],'roisize',roisize);
%                 end
%             else
%                 srpdeOut = dir([fixedDataFolder folders(i).name '\srpdeMetrics_2x.mp4']);
%                 if ~overwrite && ~isempty(srpdeOut) && srpdeOut.bytes ~=0
%                     fprintf('%s already processed, skipping... \n',folders(i).name);
%                     skip = true;
%                 end
%                 %                 if(overwrite || ~skip)
%                 %                     d = folders(i);
%                 %                     fprintf('Use butterflow to interpolate on %s%s\\ \n',fixedDataFolder,d.name);
%                 %                     command = ['butterflow -v -r 3x ' fixedDataFolder d.name '\srpdeMetrics.mp4 -o ' fixedDataFolder d.name '\srpdeMetrics_3x.mp4'];
%                 %                     fprintf('Executing : %s\n',command);
%                 %                     [status,cmdout] = system(command,'-echo');
%                 %                 end
%             end
%         end
%     end
end
end

