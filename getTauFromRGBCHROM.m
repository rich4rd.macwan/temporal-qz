function [lags, chromTrace, wChrom] = getTauFromRGBCHROM(RGB,Fs, prevChromTrace, L, gtTrace)
LOW_F = .7; UP_F = 3;
[chromTrace, wChrom] = getChromTrace(RGB);
chromTrace = smooth(chromTrace)';
% normalization to remove DC

tmp = zeros(size(chromTrace));
for t = 1 : size(chromTrace,2)-L+1
    C =  chromTrace(:,t:t+L-1);
    tmp(:, t:t+L-1) =  tmp(:, t:t+L-1) + diag(mean(C,2))^-1 * C-1;
end
chromTrace = tmp;
clear tmp;


% chromTrace = chromTrace/std(chromTrace);
if ~isempty(prevChromTrace)
    chromTrace = chromTrace + prevChromTrace;
end
N = length(chromTrace)*3;
rPPG_freq = [0 : N-1]*Fs/N;
rPPG_power = abs(fft(chromTrace,N)).^2;

rangeRPPG = (rPPG_freq>LOW_F & rPPG_freq < UP_F);   % frequency range to find PSD peak
rPPG_power = rPPG_power(rangeRPPG);
rPPG_freq = rPPG_freq(rangeRPPG);
[pksrPPG,loc] = findpeaks(rPPG_power,'SortStr','descend','NPeaks',2); % get 2 first peaks (only one is used now but will be useful for postpreocessing
rPPG_peaksLoc = rPPG_freq(loc);
HR = rPPG_peaksLoc(1);
tau = 1/HR;
lags = round(tau * Fs);


PPG_power = abs(fft(chromTrace,N)).^2;

PPG_power = PPG_power(rangeRPPG);

[pksPPG,loc] = findpeaks(PPG_power,'SortStr','descend','NPeaks',2); % get 2 first peaks (only one is used now but will be useful for postpreocessing
PPG_peaksLoc = rPPG_freq(loc);
HR_PPG = PPG_peaksLoc(1);
tau_PPG = 1/HR_PPG;
lags_PPG = round(tau_PPG * Fs);
end
