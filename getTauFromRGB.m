function [lags, trace, rPPG_freq, rPPG_power,rPPG_peaksloc, wPVM] = getTauFromRGB(RGB,Fs)
% Get the best period corresponding to from 3XN RGB signal
X_full = RGB;
for k=1:size(X_full,1)
    X_full(k,:) = detrendsignal(X_full(k,:)')';
end

winLength_full = size(X_full,2);
% normalize each signal to have zero mean and also unit variance
X_full = X_full - repmat(mean(X_full,2),[1 winLength_full]);
X_full = X_full ./ repmat(std(X_full,0,2),[1 winLength_full]);
[lags, ~] = tabusearch(X_full,40,200,Fs);
[PM, trace, K, R, wPVM] = estimatePeriodicityMetric(X_full,lags);
[rPPG_freq, rPPG_power, rPPG_peaksloc] = fftrPPG_RGB(X_full,Fs, lags);


end
