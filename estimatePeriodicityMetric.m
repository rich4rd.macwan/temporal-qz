function [PM, Y, K, R, bestW] = estimatePeriodicityMetric(X_full,nLags)
% Estimate Px, Cx
K = 1;
R = 2;
for I=1:K
    [Px, Cx] = estimatePeriodicityMatrix(nLags, X_full);
    
    % W = GEVD(Px, Cx)
    [V,D,W_]  = eig(Px,Cx);
    D = diag(D);
    %% Use qz instead of eig
     [~, indices] = sort(-1*(D));
    D = D(indices);
    W = W_(:,indices);
    bestW = W(1,:);
    y = W'*X_full;
    s = y;

    X_full = W'\s;
end


% Periodicty measure
Y = (s(1 ,1:end-nLags));
Yt =(s(1,nLags+1:end));
PM = 100*abs(trace(Y*Yt')/trace(Y*Y'));
