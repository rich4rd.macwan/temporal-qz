function [E,F,X] = householder_r_post_block(E,F,r,k,col,wantx,X,S,N,varargin)
%NOTE: This function is deprecated, not maintained!Just here for backward
%compatibility
%%Find householder reflections to eliminate elements k+1:k+r-1 of column
%%col
%%SxS = block size and NxN is the number of blocks
%%Total size of matrix is SNxSN
%%All the calculations below will be done as if it were a matrix of NxN


nFixedArgs = 9;
mask = [];
if nargin >nFixedArgs
    for i = 1 : nargin-nFixedArgs
        if strcmp(varargin(i),'mask')
            mask = varargin{i+1};
        end
    end
end

%Block equivalent parameters
cols = numel(col:col+r-1);
nrows = size(E,1)/S;
%mask is only S*S, rep it to work with the whole matrices
mask = repmat(mask,[nrows N]);
COL = (col-1)*S+1;
R=S*r;
K = k*S+1:(k+1)*S;

%normx = norm(E(k+1,col:col+r-1));
%Reshape the block column vector into a block column vector in the third
%dim
%https://www.dropbox.com/s/jmlv46ueig1bjex/reshapeForNormXHouseholder.mp4

EE = E(K, COL:COL+R-1);
FF = F(K, COL:COL+R-1);
szEE = size(EE);
blk=reshape(EE,[S S r]);
normx = sqrt(sum(blk.^2,3));

%s = -sign(E(k+1,col+r-1));
s = -sign(EE(:,end-S+1:end));
%u1 = E(k+1,col+r-1) - s*normx;
u1 = EE(:,end-S+1:end) - s.*normx ;

%w = E(k+1,col:col+r-1)/u1;
%[N-k r-1]
%w = EE(:,1:(r-1)*S)./repmat(u1,[1 N-k]);
w = EE(:,1:(r-1)*S)./repmat(u1,[1 r-1]);
w = [w ones(S)];

tau = -s.*u1./normx;

% E := Qk*E
%W_TAU = w.*repmat(tau,[1 N-k+1]);
W_TAU = w.*repmat(tau,[1 r]);
E_WT_TAU = zeros(N*S,szEE(2));
F_WT_TAU = zeros(N*S,szEE(2));
if(wantx)
    X_WT_TAU = zeros(N*S,szEE(2));
end
for i = 1:cols
    wi = w(:,(i-1)*S+1:i*S);
    for j=1:r
        Wi_TAU = W_TAU(:,(j-1)*S+1:j*S);
        %Column multiplication
        Wi_TAU_W = repmat(Wi_TAU.*wi,[N 1]);
        %Multiply by each block column of E and save to add up later
        ej = E(:,(COL-1)+(j-1)*S+1:(COL-1)+j*S);
        fj = F(:,(COL-1)+(j-1)*S+1:(COL-1)+j*S);
        
        E_WT_TAU(:,(i-1)*S+1:i*S) = ...
                 E_WT_TAU(:,(i-1)*S+1:i*S)+ ej.*Wi_TAU_W;
        F_WT_TAU(:,(i-1)*S+1:i*S) = ...
                 F_WT_TAU(:,(i-1)*S+1:i*S)+ fj.*Wi_TAU_W;
        if(wantx)
            xj = X(:,(COL-1)+(j-1)*S+1:(COL-1)+j*S);
            X_WT_TAU(:,(i-1)*S+1:i*S) = ...
                 X_WT_TAU(:,(i-1)*S+1:i*S)+ xj.*Wi_TAU_W;
        end
    end
end
if ~isempty(mask)
    E(:,COL:COL+R-1) = E(:,COL:COL+R-1)- E_WT_TAU.*mask(:,COL:COL+R-1);
    % F := Qk*F
    F(:,COL:COL+R-1) = F(:,COL:COL+R-1)- F_WT_TAU.*mask(:,COL:COL+R-1);
    if (wantx)
        X(:,COL:COL+R-1) = X(:,COL:COL+R-1)- X_WT_TAU.*mask(:,COL:COL+R-1);
    end

else
    E(:,COL:COL+R-1) = E(:,COL:COL+R-1)- E_WT_TAU;
    % F := Qk*F
    F(:,COL:COL+R-1) = F(:,COL:COL+R-1)- F_WT_TAU;
    if (wantx)
        X(:,COL:COL+R-1) = X(:,COL:COL+R-1)- X_WT_TAU;
    end
end
%Reset the NaNs introduced due to already zeroed out elements in A
E(isnan(E))=0;
F(isnan(F))=0;
end