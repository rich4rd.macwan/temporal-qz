# Temporal QZ 

This repository contains the code for Temporal QZ for the article "Generalized Eigenvalue Decomposition in Higher Dimensions applied to Estimation of Spatial rPPG Distribution of skin".

DEPENDECIES
-----------
- Matlab module find_face_landmarks found at github.com/YuvalNirkin/find_face_landmarks.
- Fast Matrix Multiplication MTIMESX by James Tursa https://fr.mathworks.com/matlabcentral/fileexchange/25977-mtimesx-fast-matrix-multiply-with-multi-dimensional-support (Compiled mexw64 module provided)

Instructions
------------
The main file for Spatial rPPG Distribution Estimation is `SrPDEV.m`, which has the path to a single video hard-coded. The file `launcherSrPDEV.m` contains code to automate the process of running SrPDEV on different videos, and requires the path to the folder containing the videos.
The entire dataset of the videos and the corresponding analysis videos are accessible on request. A sample video showing the result of SrPDE is shown below. Contact us for more metrics videos.
![Sample SrPDE metrics](sample-SrPDEMetrics.mp4)


NOTE: See the file [link_to_metrics_videos] for all the metrics videos for the databases used in the evaluation.

Author:
Richard Macwan
rich4rd.macwan@gmail.com
