function [zr,zi]=CDIV_temporal(xr,xi,yr,yi)
% COMPLEX DIVIDE. Z = X/Y, subroutine adapted from origin QZ article
%C . B . Moler and G . W . Stewart, �An Algorithm for Generalized
%Matrix Eigenvalue Problems�
xr=squeeze(xr); 
xi=squeeze(xi);
yr = squeeze(yr);
yi = squeeze(yi);


wr = zeros(size(xr));
wi = wr;
zr = wr;
zi = wr;
vi = wr;
vr = wr;
d = wr;

%For all of abs(yr)>=abs(yi) do this
mask = abs(yr) >= abs(yi);
wr(mask) = xr(mask)./yr(mask);
wi(mask) = xi(mask)./yr(mask);
vi(mask) = yi(mask)./yr(mask);
d(mask) = 1. + vi(mask).*vi(mask);

zr(mask) =(wr(mask) + wi(mask).*vi(mask))./d(mask);
zi(mask) =(wi(mask) - wr(mask).*vi(mask))./d(mask);


wr(~mask) = xr(~mask)./yi(~mask);
wi(~mask) = xi(~mask)./yi(~mask);
vr(~mask) = yr(~mask)./yi(~mask);
d(~mask) = vr(~mask).*vr(~mask) + 1.;
zr(~mask) =(wr(~mask).*vr(~mask) + wi(~mask))./d(~mask);
zi(~mask) =(wi(~mask).*vr(~mask) - wr(~mask))./d(~mask);

end
