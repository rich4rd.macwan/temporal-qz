function [z_detrended] = detrendsignal(zorig)
%Detrending method based on smoothness priors approach
%z has to be a column vector based on: Tarvainen et al., 
%"An advanced detrending method with application to hrv analysis".
z = zorig;
sz = size(z);
if numel(sz)>2
    %Detrending multidim array. For the moment, hxwxn arrays are supported
    if numel(sz)>3
        error('detrendsignal(): Only arrays upto 3 dimensions are supported for the moment');
    end
    % Reshape into n x h*w array
    z=reshape(z,[],size(z,3))';
    T = sz(3);
elseif numel(sz)==2
    T=sz(1);
else
    T = length(z); 
    z = z(:);
end
    
    %Regularizer
    lambda = 500; 
    I = speye(T);
    %The second order difference matrix
    D2 = spdiags(ones(T-2,1)*[1 -2 1],(0:2),T-2,T); 
    %The detrended signal
    z_detrended = (I-inv(I+lambda^2.*(D2'*D2)))*z;
    if numel(sz)>2
        %Reshape z_detrended
        z_detrended = reshape(z_detrended',sz(1),sz(2),[]);
    end


end

