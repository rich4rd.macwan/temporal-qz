% Wrapper class to reade normal video files or UBFC-RPPG dataset files
% Usage: UBFCRPPGVideoReader(fileName, varargin)
% fileName can be a video file, or a path to a UBFC-RPPG dataset folder
% 
% varargin can be name value pairs:
% 'Dataset': 'MMSE-HR' for use with the MMSE-HR dataset
% 'Format': can be any of the video formats. 'bmp' for UBFC-RPPG
% dataset
 
classdef (CaseInsensitiveProperties=true, TruncatedProperties=true)...
        UBFCRPPGVideoReader < hgsetget & matlab.mixin.CustomDisplay
    properties(GetAccess='public')
        Name            % Name of the file to be read.
        Path            % Path of the file to be read.
        Database        %Database MMSE-HR or IUT or LE2I
        Duration        % Total length of file in seconds.
        FrameRate       %FrameRate
        nFrames         %Nb of frames
        Format       %avi,jpg,bmp,ppm
        VideoReader     %Ref to VideoReader obj
        Height          %Frame height
        Width           %Frame width
        currentFrameNumber = 1
        currentFrame
    end
    
    %------------------------------------------------------------------
    % Documented methods
    %------------------------------------------------------------------    
    methods(Access='public')
    
        %------------------------------------------------------------------
        % Lifetime
        %------------------------------------------------------------------
        function obj = UBFCRPPGVideoReader(fileName,varargin)

            % If no file name provided.
            if nargin == 0
                error(message('MATLAB:audiovideo:VideoReader:noFile'));
            end
            
            try
                validateattributes(fileName, {'char'}, {'row', 'vector'}, 'VideoReader');
            catch ME
                throwAsCaller(ME);
            end
            
            %Set properties
            i = 2;
            obj.Database = '';
            obj.Name = 'vid.avi';
            while(nargin > i)
                if(strcmp(varargin{i-1},'database')==1)
                    obj.Database=varargin{i};
                end
                if(strcmp(varargin{i-1},'format')==1)
                    obj.Format=varargin{i};
                end
                if(strcmp(varargin{i-1},'name')==1)
                    obj.Name=varargin{i};
                end
                i = i+2;
            end
            
            obj.Path = fileName;
            
            if isempty(obj.Format)
                %Guess format from the video extension
                tokens = strsplit(obj.Path,'.');
                %Strip empty characters
                tokens = tokens(~cellfun('isempty',tokens));
                obj.Format = tokens{end};
            end
            %Default behaviour of VideoReader
            if strcmp(obj.Format,'bmp')~=1 && strcmp(obj.Format,'jpg')~=1 && strcmp(obj.Format,'ppm')~=1
                if isempty(obj.Database)
                    obj.videoReader = VideoReader([ obj.Path obj.Name]);
                else
                    obj.videoReader = VideoReader([ obj.Path 'vid.avi']);
                end
                obj.FrameRate = obj.videoReader.FrameRate;
                obj.Duration = obj.videoReader.Duration;
                obj.Width = obj.videoReader.Width;
                obj.Height = obj.videoReader.Height;
                obj.nFrames = obj.FrameRate*obj.Duration;
            else
                if strcmp(obj.Database,'MMSE')==1
                    obj.Format = 'jpg';
                    obj.nFrames = length(dir([fileName '/*.jpg']));
                    obj.FrameRate = 25; %Fixed framerate for MMSE
                    obj.Duration = obj.nFrames / obj.FrameRate;
                else
                    if strcmp(obj.Format,'bmp')==1 ||strcmp(obj.Format,'ppm')==1 
                        %Path check
                        if strcmp(fileName(end),'/')~=1 && strcmp(fileName(end),'\')~=1
                            fileName = [fileName '/'];
                        end
                        obj.Path = [fileName 'img/'];
                        d = dir([obj.Path '*.' obj.Format]);
                        obj.nFrames = length(d);
                        %Last frame
                        lastFrame = dir([obj.Path 'frame_' num2str(obj.nFrames-1) '*.' obj.Format]);
                        tokens = strsplit(lastFrame.name,'_');
                        tokens = strsplit(tokens{3},'.');
                        obj.Duration = str2double(tokens{1})/1000;
                        
                        %Extract average framerate
                        names = extractfield(d,'name');
                        durs = cellfun(@(x) strsplit(x(7:end-4),'_'),names,'UniformOutput',false);
                        timestamps = sort(cell2mat(cellfun(@(x) str2double(x{2}),durs,'UniformOutput',false)));
                        obj.FrameRate = mean(diff(timestamps)); 
                        im = imread([obj.Path lastFrame.name]);
                        obj.Width = size(im,2);
                        obj.Height = size(im,1);

                    end
                end
            end
            
        end

        %------------------------------------------------------------------
        % Operations
        %------------------------------------------------------------------        
        inspect(obj)
        
        varargout = readFrame(obj, varargin)
        eof = hasFrame(obj)
        
        %------------------------------------------------------------------        
        % Overrides of builtins
        %------------------------------------------------------------------ 
        c = horzcat(varargin)
        c = vertcat(varargin)
        c = cat(varargin)
    end
    
    methods
        function set.Duration(obj,value)
            obj.Duration = value;
        end
        function set.FrameRate(obj,value)
            obj.FrameRate= value;
        end
        function set.Name(obj,value)
            obj.Name= value;
        end
        function set.Path(obj,value)
            obj.Path= value;
        end
        function set.nFrames(obj,value)
            obj.nFrames= value;
        end
        
        function value = get.Duration(obj)
            value = obj.('Duration');
        end
        
        function value = get.FrameRate(obj)
            value = obj.('FrameRate');
        end
        
        function value = get.Name(obj)
            value = obj.('Name');
        end
                
        function value = get.Path(obj)
            value = obj.('Path');
        end
                
        function value = get.nFrames(obj)
            value = obj.('nFrames');
        end
        
        function value = get.currentFrameNumber(obj)
            value = obj.('currentFrameNumber');
        end
        
        function value = get.currentFrame(obj)
            value = obj.('currentFrame');
        end
        
    end
end