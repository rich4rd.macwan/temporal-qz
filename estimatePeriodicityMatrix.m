function [Px, Cx] = estimatePeriodicityMatrix(nLags, X_full)
%% Extract the periodicity matrix
X = X_full(:,1:end-nLags);
Xt = X_full(:,nLags+1:end);
N = size(X,2);
Px = 1/(N-1)*(X*Xt');
Cx = 1/(N-1)*(X*X');

%Maintain symmetry
Px = (Px + Px')/2;
Cx = (Cx + Cx')/2;
end