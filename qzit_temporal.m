function [A,B,epsa,epsb,X]=qzit_temporal(A,B,eps_ml,wantx,X)
%%S is the size of each block, N is the number of blocks
%%Perform the iteration strategy to reduce A to block quasi-triangular
%%while keeping B block triangular
%A must be of size Nx(N*S*S)
sz = size(A);
S = abs(sqrt(sz(3)));
initA = A;
%Verify block sizes are correct
%assert (sz(1)~=N ||sz(2)~=N||size(3)~=S*S,'Please pass appropriate sized matrix and block size parameters.A should be of size N x(S*S)xN');

%%     INITIALIZE ITER, COMPUTE EPSA,EPSB

anorm=max(sum(abs(A),2));
bnorm=max(sum(abs(B),2));

epsa = eps_ml.*anorm;
epsb = eps_ml.*bnorm;

%%     REDUCE A TO QUASITRIANGULAR, KEEP B TRIANGULAR
n=sz(1);
m=sz(1);
iter = zeros(m,sz(3));
ITER = iter;
%Index array representing the matrices that still need to be reduced
%Those which are reduced will be replaced back into initA and removed from
%this matrix. E.g if the 4th matrix is reduced, then we'll have
%indices =[1 2 3 5 ...S*S]. Only these remaining matrices will be then used
%for all subsequent reductions
indices = ones(1,sz(3),'logical');
old1 = abs(A(m,m-1,:));
old2 = abs(A(m-1,m-2,:));
A10 = zeros(1,1,sz(3));
A20 = zeros(1,1,sz(3));
A30 = zeros(1,1,sz(3));
AL1LZeroMask = [];

idx=0;
while(m>2)
    %CHECK FOR CONVERGENCE OR REDUCIBILITY
    %Remove the matrices which have already been reduced and place them
    %in initA
    idx = idx +1;
    for l = m:-1:2
        contOuter = false;
        %Check the matrices which have any of their subdiagonal values<=epsa.
        all1zeroMask = squeeze(abs(A(l,l-1,:))<=epsa);
        %Set them to zero, remove the matrix from the list and continue
        A(l,l-1,all1zeroMask) = 0;
        %Save the reduced matrices in initA
        initA(:,:,all1zeroMask) = A(:,:,all1zeroMask);
        %Remove the reduced matrices and their indices
        if any(all1zeroMask)
            %             A(:,:,all1zeroMask) = [];
            %             B(:,:,all1zeroMask) = [];
            %             epsa(:,:,all1zeroMask) = [];
            %             epsb(:,:,all1zeroMask) = [];
            %             old1(all1zeroMask) = [];
            %             old2(all1zeroMask) = [];
            %             iter(:,all1zeroMask) = [];
            indices(all1zeroMask) = 0;
        end
        %Decrement m when all the matrices have their subdiagonals reduced to
        %zero
        if ~any(indices)
            m = l-1;
            contOuter = true;
            break;
        else
           %Remove nans 
           indices(isnan(A(m,m-1,:))) = 0;
        end
    end
    
    l=1;
    if contOuter
        continue
    end
    
    %     CHECK FOR SMALL TOP OF B
    smallTopBMask = squeeze(abs(B(l,l,:))<=epsb);
    B(l,l,smallTopBMask) = 0;
    if any(smallTopBMask)
        A_ = A(:,:,smallTopBMask); B_ = B(:,:,smallTopBMask);
        [A_,B_] = householder_r_temporal(A_,B_,2,l,l);
        A(:,:,smallTopBMask)=A_; B(:,:,smallTopBMask)=B_;
        %For the matrices which also have A(l+1,l) = 0, set increment their
        %l
        AL1LZeroMask = squeeze(abs(A(l+1,l,:)==0));
    end
    
    m1 = m - 1;
    l1 = l + 1;
    const = 0.75;
    
    %Loop vars
    ITER = iter(m,:);
    %Check for unconverged indices
    stopMask = ITER==-1;
    if numel(find(stopMask==1))
       %There are indices with unconverged values. Need to stop though
        break;
    end
    ITER(indices) = ITER(indices) + 1;
    
    %Build mask for iter(m)==1 or if the subdiagonal values reduce to less
    %than .75 of their older value
    iterMask = squeeze(ITER==1);
    cond1Mask = squeeze(abs(A(m,m1,:))<const.*old1);
    cond2Mask = squeeze(abs(A(m-1,m-2,:))<const.*old2);
    %Update global mask
    condMask = indices(:)&(iterMask(:)|cond1Mask(:)|cond2Mask(:));
    
    AA = A(:,:,condMask);    BB = B(:,:,condMask);
    XX = X(:,:,condMask);
    EPSB = epsb(condMask);
    % OLD1 = old1(condMask);   OLD2 = old2(condMask);
    
    
    
    %TODO: if AL1LZeroMask is not empty, for those elements perform the
    %following code using l=l1.
    %save currentA and B
    %Pick matrices at AL1LZeroMask indices and perform the iteration using
    %l=l1
    %Pick matrices at ~AL1ZeroMask indices and perform the iteration
    %normally
    %For the rest of the elements, do the normal iteration
    
    %     ZEROTH COLUMN OF ANORM
    if ~isempty(AL1LZeroMask)
        condMask = condMask & ~AL1LZeroMask;
    end
    
    if any(condMask)
            b11 = BB(l,l,:);
            b22 = BB(l1,l1,:);
            tempMask = abs(b22) < EPSB;
            if any(tempMask)
                b22(tempMask) = EPSB(tempMask);
            end
            
            b33 = BB(m1,m1,:);
            tempMask = abs(b33)<EPSB;
            if any(tempMask)
                b33(tempMask) = EPSB(tempMask);
            end
            
            b44 = BB(m,m,:);
            tempMask = abs(b44)<EPSB;
            if any(tempMask)
                b44(tempMask) = EPSB(tempMask);
            end
            
            a11 = AA(l,l,:)./b11;     a12 = AA(l,l1,:)./b22;
            a21 = AA(l1,l,:)./b11;	a22 = AA(l1,l1,:)./b22;
            a33 = AA(m1,m1,:)./b33;   a34 = AA(m1,m,:)./b44;
            a43 = AA(m,m1,:)./b33;    a44 = AA(m,m,:)./b44;
            b12 = BB(l,l1,:)./b22;    b34 = BB(m1,m,:)./b44;
            A10(condMask) =((a33-a11).*(a44-a11) - a34.*a43 + a43.*b34.*a11)./a21 + a12 - a11.*b12;
            A20(condMask) =(a22-a11-a21.*b12) -(a33-a11) -(a44-a11) + a43.*b34;
            A30(condMask) = AA(l+2,l1,:)./b22;
            
            
            
            a10 = A10(:,:,condMask);
            a20 = A20(:,:,condMask);
            a30 = A30(:,:,condMask);
            
            
            OLD1 = abs(AA(m,m-1,:));
            OLD2 = abs(AA(m-1,m-2,:));
            
            %BEGIN MAIN LOOP
            
            for  k=l:m1
                km1 = k-1;
                if(km1 < l)
                    km1 = l;
                end
                
                %Determine Qk in H3(k) to annihilate a(k+1,k-1) and a(k+2,k-1)
                if(k==l)
                    %H3(k) with a10,a20,a30
                    [Aaug,BB] = householder_r_temporal([[a10;a20;a30;repmat(zeros(n-3,1),1,1,size(AA,3))],...
                        AA],BB,3,k,k);
                    AA = Aaug(:,2:end,:);
                else
                    if (k > l&&k < m1)
                        %                     %H3(k) normal
                        [AA,BB] = householder_r_temporal(AA,BB,3,k,km1);
                    else
                        %H2(k) for k~=l
                        [AA,BB] = householder_r_temporal(AA,BB,2,k,km1);
                    end
                end
                
                %Determine Zk' in H3(k) to annihilate b(k+2,k+1) and b(k+2,k)
                if(k ~= m1)
                    [BB,AA,XX]= householder_r_post_temporal(BB,AA,3,k+1,k,wantx,XX);
                end
                
                %Determine Zk'' in H3(k) to annihilate b(k+2,k+1) and b(k+2,k)
                [BB,AA,XX]= householder_r_post_temporal(BB,AA,2,k,k,wantx,XX);
                
            end
            %Save the changed values
            A(:,:,condMask)=AA;    B(:,:,condMask)=BB;
            if (wantx)
                X(:,:,condMask) = XX;
            end
            
            epsa(condMask)=EPSB;   epsb(condMask)=EPSB;
            old1(condMask)=OLD1;   old2(condMask)=OLD2;
     end
    
    adhocmask = ITER==10;
    if any(adhocmask)
        A10(adhocmask) = 0.;
        A20(adhocmask) = 0.;
        A30(adhocmask) = 1.1605;
    end
    
    stopMask = ITER>12;
    if numel(find(stopMask==1))
       ITER(stopMask) = -1;
    end
    
    iter(m,indices) = ITER(indices);
    %End of one QZ Step
end
%fprintf('Temporal QZ finished in %d iterations\n',idx);
A = initA;
end