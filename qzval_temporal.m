function [A,B,alfr,alfi,beta,X]=qzval_temporal(A,B,epsa,epsb,wantx,X)
%%S is the size of each block, N is the number of blocks
%%Perform the iteration strategy to reduce A to block quasi-triangular
%%while keeping B block triangular
%A must be of size Nx(N*S*S)
sz = size(A);
S = abs(sqrt(sz(3)));
initA = A;
%Verify block sizes are correct
%assert (sz(1)~=N ||sz(2)~=N||size(3)~=S*S,'Please pass appropriate sized matrix and block size parameters.A should be of size N x(S*S)xN');


alfr = zeros(sz(1),sz(3));
alfi = zeros(sz(1),sz(3));
beta = zeros(sz(1),sz(3));

%Find the 2x2 blocks in the tensor A
% x x x         x x x
% 0 x x    or   x x x
% 0 x x         0 0 x
n=sz(2);
m=n;
%TODO: for m==1
cumMask = true(1,sz(3));
while(m>0)
    
    l = m-1;
    if(m==1)
        mask = alfr(m,:)==0;
        alfr(m,mask) = A(m,m,mask);
        beta(m,mask) = B(m,m,mask);
        alfi(m,mask) = 0;
        m= m-1;
    else
        zmm1 = false(1,sz(3));
        zmm1(A(m,m-1,:) <epsa) = true;
        zmm1 = zmm1 & cumMask;
        alfr(m,zmm1) = A(m,m,zmm1);
        beta(m,zmm1) = B(m,m,zmm1);
        alfi(m,zmm1) = 0.;
        %Save this mask, elements that had m,m-1 ==0, i.e. subblock at 1,1,
        %3,3 eigenval calculated
        %Advance for the others
        ncm = cumMask & ~zmm1;
        
        %ncm are going to have complex eigenvalues at m,m-1
        
        idx = ncm & squeeze(abs(B(l,l,:))<=epsb)';
        if any(idx)
            B(l,l,idx) = 0;
            [AA, BB] = householder_r_temporal(A(:,:,idx),B(:,:,idx),2,l,l);
            A(:,:,idx) = AA; B(:,:,idx)=BB;
        end
        
        
        idx2 = ncm & ~idx & squeeze(abs(B(m,m))<=epsb)';
        B(m,m,idx2) = 0;
        if any(idx2) && m<sz(2)
            [AA, BB, XX] = householder_r_post_temporal(A(:,:,idx2),B(:,:,idx2),2,m,l,wantx,X(:,:,idx2));
            A(:,:,idx2) = AA; B(:,:,idx2)=BB;
            X(:,:,idx2) = XX;
        end
        
        idx3 = ncm & ~idx2;
        %p==i correspond to subblocks at i,i
        
        Ai = A(:,:,idx3);
        Bi = B(:,:,idx3);
        if (wantx)
            Xi = X(:,:,idx3);
        end
        alfri = alfr(:,idx3);
        alfii = alfi(:,idx3);
        betai = beta(:,idx3);
        
        %The special conditions are taken care of, now do the 2x2 subblocks
        an = abs(Ai(l,l,:))+abs(Ai(l,m,:))+abs(Ai(m,l,:))+abs(Ai(m,m,:));
        bn = abs(Bi(l,l,:))+abs(Bi(l,m,:))+abs(Bi(m,m,:));
        a11 = Ai(l,l,:)./an;
        a12 = Ai(l,m,:)./an;
        a21 = Ai(m,l,:)./an;
        a22 = Ai(m,m,:)./an;
        b11 = Bi(l,l,:)./bn;
        b12 = Bi(l,m,:)./bn;
        b22 = Bi(m,m,:)./bn;
        c =(a11.*b22 + a22.*b11 - a21.*b12)./2.;
        d =(a22.*b11 - a11.*b22 - a21.*b12).^2./4. + a21.*b22.*(a12.*b11 - a11.*b12);
        
        %% For the indices where d>=0, we have real roots.
        dgz = d>=0;
        if (any(dgz))
             e =(c(dgz) + sign(c(dgz)).*sqrt(d(dgz)))./(b11(dgz).*b22(dgz));
            a11(dgz) = a11(dgz) - e.*b11(dgz);
            a12(dgz) = a12(dgz) - e.*b12(dgz);
            a22(dgz) = a22(dgz) - e.*b22(dgz);
            
            u1 = zeros(size(a11(dgz)));
            u2 = u1;
            v1 = u1;
            v2 = u1;
            
            E_dgz = [a11(dgz), a12(dgz);a21(dgz), a22(dgz)];
            flip =(abs(a11(dgz))+abs(a12(dgz))) >=(abs(a21(dgz))+abs(a22(dgz)));
            
            if any(flip)
                %HSH2 on the first row of E_dgz and populate u and v for those indices
                [u1(flip), u2(flip),v1(flip),v2(flip)] = ...
                    HSH2_temporal(E_dgz(1,2,flip),E_dgz(1,1,flip));
            end
            %HSH2 on the second row of E_dgz and populate u and v for those indices
            if any(~flip)
                [u1(~flip), u2(~flip),v1(~flip),v2(~flip)] = ...
                    HSH2_temporal(E_dgz(2,2,~flip),E_dgz(2,1,~flip));
            end
            mask = squeeze(u1==1);
            
            %For all the u1s == 1, apply the HSH2 transformation
            A_dgz = Ai(:,:,dgz); B_dgz = Bi(:,:,dgz);
            if (wantx)
                X_dgz = Xi(:,:,dgz);
            end
            %     p_dgz = p(dgz);
            
            
            %When subblock is at 1,1 position
            U2 = repmat(u2(mask),sz(1),1);
            V1 = repmat(v1(mask),sz(1),1);
            V2 = repmat(v2(mask),sz(1),1);
            
            t = A_dgz(:,m,mask ) + U2.*A_dgz(:,l,mask);
            A_dgz(:,m,mask) =  A_dgz(:,m,mask )  + V1.*t;
            A_dgz(:,l,mask) =  A_dgz(:,l,mask )  + V2.*t;
            
            t = B_dgz(:,m,mask) + U2.*B_dgz(:,l,mask);
            B_dgz(:,m,mask) =  B_dgz(:,m,mask)  + V1.*t;
            B_dgz(:,l,mask) =  B_dgz(:,l,mask)  + V2.*t;
            if wantx
                t = X_dgz(:,m,mask) + U2.*X_dgz(:,l,mask);
                X_dgz(:,m,mask) =  X_dgz(:,m,mask)  + V1.*t;
                X_dgz(:,l,mask) =  X_dgz(:,l,mask)  + V2.*t;
                Xi(:,:,dgz) = X_dgz;
                X(:,:,idx3) = Xi;
            end
            
            
            flip = squeeze(bn(dgz)~=0 & an(dgz)>=abs(e).*bn(dgz));
            mf = mask & flip;
            mnf = mask & ~flip;
            
            %For all the subblocks at positions 1,1
            [BB, AA] = householder_r_temporal(B_dgz(:,:,mf),A_dgz(:,:,mf),2,l,l);
            A_dgz(:,:,mf) = AA; B_dgz(:,:,mf)=BB;
            
            %For all the subblocks at positions 1,1 , for ~flip
            [AA, BB] = householder_r_temporal(A_dgz(:,:,mnf),B_dgz(:,:,mnf),2,l,l);
            A_dgz(:,:,mnf) = AA; B_dgz(:,:,mnf)=BB;
            
            A_dgz(m,l,:) = 0;
            B_dgz(m,l,:) = 0;
            Ai(:,:,dgz) = A_dgz; Bi(:,:,dgz) = B_dgz;
            
            alfri(l,dgz) = Ai(l,l,dgz);
            alfri(m,dgz) = Ai(m,m,dgz);
            alfii(m,dgz) = 0;
            betai(l,dgz) = Bi(l,l,dgz);
            betai(m,dgz)  = Bi(m,m,dgz);
            
            A(:,:,idx3) = Ai; B(:,:,idx3) = Bi;
            alfr(:,idx3) = alfri;
            beta(:,idx3) = betai;
            alfi(:,idx3) = alfii;
            
            %m = m-1;
            
        end
        %% For the others, we have complex roots
        dlz = ~dgz;
        if(any(dlz))
            a11_ = a11(dlz); a12_ = a12(dlz); a22_ = a22(dlz); a21_ = a21(dlz);
            b11_ = b11(dlz);  b22_ = b22(dlz);
            b12_ = b12(dlz);
            er = c(dlz)./(b11_.*b22_);
            ei = sqrt(-d(dlz))./(b11_.*b22_);
            a11r = a11(dlz) - er.*b11_;
            a11i = ei.*b11_;
            a12r = a12(dlz) - er.*b12_;
            a12i = ei.*b12_;
            a21r = a21(dlz);
            a21i = 0*a21(dlz);
            a22r = a22_ - er.*b22_;
            a22i = ei.*b22_;
            
            flip =(abs(a11r)+abs(a11i)+abs(a12r)+abs(a12i)) >=(abs(a21r)+abs(a22r)+abs(a22i));
            cz = zeros(size(a11r));
            szr = cz;
            szi = cz;
            cq = cz;
            sqr = cz;
            sqi = cz;
            [cz(flip),szr(flip),szi(flip)]=CHSH2_temporal(a12r(flip),a12i(flip),-a11r(flip),-a11i(flip));
            
            [cz(~flip),szr(~flip),szi(~flip)]=CHSH2_temporal(a22r(~flip),a22i(~flip),-a21r(~flip),-a21i(~flip));
            
            flip = an(dlz) >=(abs(er)+abs(ei)).*bn(dlz);
            [cq(flip), sqr(flip),sqi(flip)]=CHSH2_temporal(cz(flip).*b11_(flip)+...
                szr(flip).*b12_(flip), szi(flip).*b12_(flip), szr(flip).*b22_(flip),...
                szi(flip).*b22_(flip));
            [cq(~flip), sqr(~flip), sqi(~flip)]=CHSH2_temporal(cz(~flip).*a11_(~flip)+...
                szr(~flip).*a12_(~flip), szi(~flip).*a12_(~flip),...
                cz(~flip).*a21_(~flip)+szr(~flip).*a22_(~flip), szi(~flip).*a22_(~flip));
            
            
            ssr = sqr.*szr + sqi.*szi;
            ssi = sqr.*szi - sqi.*szr;
            tr = cq.*cz.*a11_ + cq.*szr.*a12_ + sqr.*cz.*a21_ + ssr.*a22_;
            ti = cq.*szi.*a12_ - sqi.*cz.*a21_ + ssi.*a22_;
            
            bdr = cq.*cz.*b11_ + cq.*szr.*b12_ +  ssr.*b22_;
            bdi = cq.*szi.*b12_ + ssi.*b22_;
            r = sqrt(bdr.*bdr + bdi.*bdi);
            
            TB = tr.*bdr + ti.*bdi;
            TBC = tr.*bdi - ti.*bdr;
            
            betai(l,dlz) = bn(dlz).*r;
            alfri(l,dlz) = an(dlz).*(TB)./r;
            alfii(l,dlz) = an(dlz).*(TBC)./r;
            
            
            tr = ssr.*a11_ - sqr.*cz.*a12_ - cq.*szr.*a21_ + cq.*cz.*a22_;
            ti = -ssi.*a11_ - sqi.*cz.*a12_ + cq.*szi.*a21_;
            bdr = ssr.*b11_ - sqr.*cz.*b12_ + cq.*cz.*b22_;
            bdi = - ssi.*b11_ -sqi.*cz.*b12_;
            r = sqrt(bdr.*bdr + bdi.*bdi);
            TB = tr.*bdr + ti.*bdi;
            TBC = tr.*bdi - ti.*bdr;
            
            betai(m,dlz) = bn(dlz).*r;
            alfri(m,dlz) = an(dlz).*(TB)./r;
            alfii(m,dlz) = an(dlz).*(TBC)./r;
            
            beta(l:m,idx3) = betai(l:m,:);
            alfr(l:m,idx3) = alfri(l:m,:);
            alfi(l:m,idx3) = alfii(l:m,:);
        end
        m=m-1;
        cumMask = cumMask & ~idx3;
    end
    
end

end
