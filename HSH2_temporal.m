function [u1,u2,v1,v2] = HSH2_temporal(a1,a2)
%Householder reflection, subroutine adapted from origin QZ article
%C . B . Moler and G . W . Stewart, �An Algorithm for Generalized
%Matrix Eigenvalue Problems�
u1=[];
u2=[];
v1=[];
v2=[];
if numel(a1)>0
    s = abs(a1) + abs(a2);
    u1 = a1./s;
    u2 = a2./s;
    r = sqrt(u1.*u1 + u2.*u2);
    r(u1<0) = -1*r(u1<0);
    v1 = -(u1 + r)./r;
    v2 = -u2./r;
    u1(:) = 1;
    u2 = v2./v1;
    u1(a2==0) = 0;
end
end