function SrPDEV(varargin)
close all
addpath('../');
WINDOW_LENGTH_SEC = 10;
vidFolder = '/run/media/richard/TOSHIBA EXT/LabData/fixedData20122017/2017_12_20-16_20_08/';
STEP_SEC = .2;


i=1;
while(nargin > i)
    if(strcmp(varargin{i},'vidFolder')==1)
        vidFolder=varargin{i+1};
    end
    if(strcmp(varargin{i},'WINDOW_LENGTH_SEC')==1)
        WINDOW_LENGTH_SEC=varargin{i+1};
    end
    if(strcmp(varargin{i},'STEP_SEC')==1)
        STEP_SEC=varargin{i+1};
    end
    i=i+2;
end


%Face Landmark detection init
%addpath('J:/src/find_face_landmarks/interfaces/matlab/');
%modelFile='J:/src/find_face_landmarks/shape_predictor_68_face_landmarks.dat';
addpath('/usr/local/interfaces/matlab/');
modelFile='/home/richard/src/IBIS_Temporal/shape_predictor_68_face_landmarks.dat';
useFaceLandmarks = true;


video = UBFCRPPGVideoReader(vidFolder,'format','avi','name','vid.avi');
normalisedSize=[580 580];
%Eyes at .3*W,H/3 and .7*W,H/3
outPts =round( [.3*normalisedSize(1) normalisedSize(2)/3;
    .7*normalisedSize(1) normalisedSize(2)/3]);

Fs = video.FrameRate;
Ts = 1/Fs;
winLength = pow2(floor(log2(WINDOW_LENGTH_SEC*Fs))); % length of the sliding window for FFT

halfWin = (winLength/2);
traceSize =int32( video.FrameRate * video.Duration);
timeTrace = linspace(0,video.Duration,traceSize);
scale = 1;
LOW_F = .7; UP_F = 3;
%Mean of extracted SrPDE
pulseTrace = zeros(1,traceSize);


maxLags = round(1.5*Fs);%seconds corresponding to 40bpm
stepSize = round(STEP_SEC*Fs);
finalPlotColor = [0 .45 .74];
%if ~useFaceLandmarks
%Load face crop coordinates, used for stationary face videos
if exist([vidFolder 'faceroi.mat'],'file') == 2
    load([vidFolder 'faceroi.mat']);
    %Faceroi should be of format x,y,w,h
else
    error(['Face ROI data not found. Either use stationary' ...
        ' video or motion compensated face tracked video, with face coordinates saved in faceroi.mat']);
end

%end
%Load GT
[gtTrace, gtHR, gtTime ] = loadPPG(vidFolder);
gtTime = gtTime / 1000;  % in seconds
Fs_PPG = 1/mean(diff(gtTime));

H = ceil(scale*faceroi(3));
W = ceil(scale*faceroi(4));
OUT = zeros(H,W,3,winLength);
%Divide face into ROIs
roisize=[32, 32];
nHCells = int32(W/roisize(1));
nVCells = int32(H/roisize(2));

fnroimean = @(block_struct) nanmean(nanmean(block_struct.data));
fnroiexpand = @(block_struct) repmat(block_struct.data,roisize(1),roisize(2));

%T = zeros(H, W, 3, round(winLength)+maxLags);
T = zeros(nVCells, nHCells, 3, round(winLength));
%Extracted SrPDE
sPulseTrace = zeros(nVCells, nHCells, traceSize);
X_bar = zeros(nVCells, nHCells, 3);


prevChromTrace = [];
ind = 1;
if ~useFaceLandmarks
    if  ~exist([vidFolder 'skinmask.png'],'file')
        %Create skin mask
        frame = readFrameUBFCRPPG(video);
        imshow(frame);
        pts = ginput();
        %Close the mask
        pts = [pts;pts(1,:)];
        skinmask = roipoly(frame,pts(:,1),pts(:,2));
        imshow(uint8(skinmask).*frame);
        pause(1);
        imwrite(skinmask,[vidFolder 'skinmask.png']);
        %Reset video
        video.VideoReader.CurrentTime = 0;
        close all;
    end
    skinmask = imread([vidFolder 'skinmask.png']);
    skinmask = (skinmask>.5);
    osz=faceroi(3:4);
end


saveHighdef=false;
displayOn = false;
if saveHighdef
    X_orig_bar = zeros(osz(1),osz(2),3);
    T_orig = zeros(osz(1),osz(2), 3, round(winLength));
end

videowriter = VideoWriter([vidFolder 'rppgOut.avi'],'Uncompressed AVI');
videowriter.FrameRate = Fs;
open(videowriter);
pointTracker = vision.PointTracker;
nWindows = numel(halfWin:stepSize:traceSize-halfWin-maxLags);
HR_PPG = zeros(1,nWindows);
HR_rPPG_win = zeros(size(T,1),size(T,2),nWindows);
HR_rPPG_avg = zeros(1,nWindows);
SNRpx_win = zeros(size(T,1),size(T,2),nWindows);
MAEpx_win = zeros(size(T,1),size(T,2),nWindows);
    
for i=halfWin:stepSize:traceSize-halfWin-maxLags
    fprintf('Pass %u of %.0f\n',ind,numel(halfWin:stepSize:traceSize-halfWin-maxLags));
    if(ind==1)
        fprintf('Accumulating %d frames for the first window\n',winLength);
    end
    startInd = i-halfWin+1;
    endInd = i+halfWin;
    if endInd-maxLags<numel(gtTime)
        gtTimeWin = gtTime(startInd:endInd-maxLags);
        gtTraceWin = gtTrace(startInd:endInd-maxLags);
    end
    
    if ind==1 %First temporal window
        %% Accumulate frames into tensor upto endInd-1
        for j = startInd:endInd
            frame = readFrameUBFCRPPG(video);
            %Landmarks and tracker
            if j==1 && useFaceLandmarks
                lmd = find_face_landmarks(modelFile, frame);
                Xs = double(lmd.faces.landmarks(:,1)');
                Ys = double(lmd.faces.landmarks(:,2)');
                %Forehead
                minX = min(Xs); minY = min(Ys);
                maxX = max(Xs); maxY = max(Ys);
                foreheadExtent = (maxY - minY)/3;
                bounds = [ minX, minX+[maxX-minX]/5, ...
                    Xs(28), maxX-[maxX-minX]/5,...
                    maxX; minY,foreheadExtent,foreheadExtent,foreheadExtent,minY];
                
                Xs = [Xs, bounds(1,:)]; Ys = [Ys, bounds(2,:)];
                %Convex hull
                k = convhull(Xs,Ys);
                points=[Xs(k)',Ys(k)'];
                initialize(pointTracker,points,frame); 
                skinmask = roipoly(frame,Xs(k), Ys(k));
%                 imshow(frame);
%                 hold on;
%                 plot(Xs, Ys,'.','MarkerSize',16);
            elseif useFaceLandmarks
                [points,validity] = pointTracker(frame);
                skinmask = roipoly(frame,points(:,1), points(:,2));
            end
            
            frame = im2double(frame);
            frame= imgaussfilt(frame, 2  );
            %frame = frame.*repmat(uint8(skinmask),1,1,3);
            frame(repmat(~skinmask,1,1,3))=-1;
            %Face crop
            frame = (frame(faceroi(1):faceroi(1)+faceroi(3)-1,...
                faceroi(2):faceroi(2)+faceroi(4)-1,:));
            
            %Face crop (manually done for the stationary videos)
            frameD = (imresize(frame,scale));
            frameD(frameD==-1) = nan;
            
            T(:,:,:,j) = blockproc(frameD,roisize,fnroimean);
            % imshow(frameBlurred);
            % title(j);
            %Accumulate original image data
            if mod(j,16)==0
                fprintf('%d | ', j);
            end
            if saveHighdef
                X_orig_bar = X_orig_bar + frame;
                T_orig(:,:,:,j) = frame;
            end
            
        end
        %Mean of X_orig_bar
        if saveHighdef
            X_orig_bar = X_orig_bar./winLength;
        end
        fprintf('\n');
    else
        %Temporal windows 2 to last. Read frames worth step seconds and update the structures
        for j = endInd-stepSize+1:endInd
            frame = readFrameUBFCRPPG(video);
            %Skinmask from tracked roi
            [points,validity] = pointTracker(frame);
            skinmask = roipoly(frame,points(:,1), points(:,2));
            
            frame = im2double(frame);
            frame= imgaussfilt(frame, 2  );
            %frame = frame.*repmat(uint8(skinmask),1,1,3);
            frame(repmat(~skinmask,1,1,3))=-1;
            %Face crop
            frame = (frame(faceroi(1):faceroi(1)+faceroi(3)-1,...
                faceroi(2):faceroi(2)+faceroi(4)-1,:));
            %Face crop (manually done for the stationary videos)
            
            frameD = (imresize(frame,scale));
            frameD(frameD==-1) = nan;
            T(:,:,:,1)= [];
            T = cat(4,T,blockproc(frameD,roisize,fnroimean));
            
            %Runnning average of original image data
            if saveHighdef
                X_orig_bar = X_orig_bar + (frame - T_orig(:,:,:,1))./winLength;
                T_orig(:,:,:,1)= [];
                T_orig = cat(4,T_orig,frame);
            end
        end
    end
    
    
    %% Update the average tensor
    X_bar = nanmean(T,4);
    
    %% Perform SrPDE here
    %Center the video data
    %Use real data for frames between winLength
    X_tilde = T - repmat(X_bar,[1 1 1 winLength]);
    
    %Extract the rPPG from averaged frames of the video 
    RGB = squeeze(nanmean(squeeze(nanmean(X_tilde,1)),1));
 
    %Blockwise PVM
    SNRpx = zeros(size(X_tilde,1),size(X_tilde,2));
   
    SNRpx_all = zeros(size(X_tilde,1),size(X_tilde,2));
    GTpx = zeros(size(X_tilde,1),size(X_tilde,2),traceSize);
    Lagspx = zeros(size(X_tilde,1),size(X_tilde,2));
    WIDTH = 0.3;
    [R,C] = ind2sub(size(skinmask),find(skinmask));
    
    %GT with noplot set to true
    [PPGlag, PPG_peaksLoc, PPG_freq,PPG_power] = fftPPG(gtTimeWin,gtTraceWin, Fs_PPG,true);
    HR_PPG(ind) = PPG_peaksLoc(1)*60;
    
    % Pseudo GT, from averaged RGB traces using PVM
    [lags, y, p_rPPG_freq, p_rPPG_power,rPPG_peaksLoc, wPVM] = getTauFromRGB(RGB,Fs);
    HR_rPPG_avg(ind) = rPPG_peaksLoc(1)*60;
    
    
    %lags = median(median(Lagspx));
    %Reshape X_tilde into a 3xNxWxW tensor
    X = X_tilde(:,:,:,1:end-lags);
    Xt = X_tilde(:,:,:,lags+1 : end);
    
    X_permuted = permute(X,[3 4 1 2]); % 3 x N x W x W
    
    Xt_permuted = permute(Xt,[4 3 1 2]); % 3 x N x W x W
    
    %Flatten last two WxW dimensions to W*W
    X_permuted = reshape(X_permuted,size(X_permuted,1),size(X_permuted,2),size(X_permuted,3)*size(X_permuted,4));
    Xt_permuted = reshape(Xt_permuted,size(Xt_permuted,1),size(Xt_permuted,2),size(Xt_permuted,3)*size(Xt_permuted,4));
    
    PHI_nan = mtimesx(X_permuted,'N',X_permuted,'T','SPEEDOMP','OMP_SET_NUM_THREADS(4)');
    PI_nan = mtimesx(X_permuted,'N',Xt_permuted,'N','SPEEDOMP','OMP_SET_NUM_THREADS(4)');
    
    notnan = ~isnan(PHI_nan);
    PHI = PHI_nan(:,:,squeeze(notnan(1,1,:)));
    PI = PI_nan(:,:,squeeze(notnan(1,1,:)));
    
    
    Vbest_nan = zeros(1,3,size(PHI_nan,3));
    [PI, PHI, alfr, alfi, beta, V, Vbest] = qz_temporal(PI,PHI,true);
    Vbest_nan(:,:,notnan(1,1,:))=Vbest;
    
    %Calculate Cx and Px tensors for tau, the period
    %Cx is pixelwise cov matrix of the whole signal
    X_prj = squeeze(mtimesx(Vbest_nan,'N',X_permuted(:,1:end,:),'SPEEDOMP','OMP_SET_NUM_THREADS(4)'));
    X_prj = reshape(permute(X_prj,[2,1]),size(X,1),size(X,2),[]);
    MAX = max(max(max(X_prj)));
    MIN = min(min(min(X_prj)));
    den = MAX-MIN;
    
   
    %Overlap add
    sPulseTrace(:,:,startInd:endInd - lags) = sPulseTrace(:,:,startInd:endInd - lags) + smooth3(X_prj(:,:,1:winLength - lags));
    pulseTrace(startInd:endInd - lags) = pulseTrace(startInd:endInd-lags) + smooth(detrendsignal(squeeze(nanmean(nanmean(sPulseTrace(:,:,startInd:endInd - lags))))))';
    
    %Calculate block wise SNR compared to PVM on average face
    %for each roi, calculate rPPG fft and compare with PPG fft
    [R, C, L] = size(X_prj);
    for r= 1:R
        [rPPG_freq, rPPG_powers, rPPG_peaksLoc] = fftrPPG(squeeze(X_prj(r,:,:)),Fs,-1,true);
        for c = 1:C
            if ~isnan(rPPG_powers(c,1))
                rPPG_power = rPPG_powers(c,:);
                
                % Get peaks rPPG
                rangeRPPG = (rPPG_freq>LOW_F & rPPG_freq < UP_F);   % frequency range to find PSD peak
                rPPG_power = rPPG_power(rangeRPPG);
                rPPG_freq = rPPG_freq(rangeRPPG);
                
                range1 = (rPPG_freq>(rPPG_peaksLoc(c,1)-WIDTH/2) & rPPG_freq < (rPPG_peaksLoc(c,1)+WIDTH/2));
                range2 = (rPPG_freq>(rPPG_peaksLoc(c,2)*2-WIDTH/2) & rPPG_freq < (rPPG_peaksLoc(c,2)*2+WIDTH/2));
                range = range1 + range2;
                signal = rPPG_power.*range;
                noise = rPPG_power.*(~range);
                SNRpx(r,c) = 10*log10(sum(signal)/sum(noise));
                HR_rPPG_win(r,c,ind) = rPPG_peaksLoc(c,1)*60;
            end
        end
    end
    
    %Normalize SNRpx
    SNRpx = SNRpx - mean(mean(SNRpx));
    SNRpx = SNRpx/std(std(SNRpx));
    SNRpx_win(:,:,ind) = SNRpx;
    
    SNRpx_all = SNRpx_all + SNRpx;
    figure(1);
    subplot(2,3,[4 5 6]);
    hold on
% 
%     if endInd-maxLags<numel(gtTime)
%         [PPGlag, PPG_peaksLoc] = getTauFromPPG(gtTimeWin,gtTraceWin, Fs_PPG);
%     end
%     [rPPG_freq,rPPG_power] = fftrPPG(pulseTrace(startInd:endInd - maxLags), Fs);

    subplot(2,3,[4 5 6]);
    grid on;
    hold on;
    for I=startInd:startInd + stepSize - 1
        %Test and fix I-startInd+1. should be just I
        frameD = X_bar +(SNRpx.*repmat(sPulseTrace(:,:,I-startInd+1),1,1,3));
        %% High def display
        if saveHighdef
            X_prj_big=blockproc(X_prj(:,:,I-startInd+1),[1 1],fnroiexpand);
            SNRbig=(blockproc(SNRpx,[ 1 1],fnroiexpand));
            %Crop to roi size
            szdelta = size(SNRbig,1) - size(X_orig_bar,1);
            SNRbig=(SNRbig(szdelta/2+1:end-szdelta/2,szdelta/2+1:end-szdelta/2));
            X_prj_big=(X_prj_big(szdelta/2+1:end-szdelta/2,szdelta/2+1:end-szdelta/2));
            frameoD = X_orig_bar + SNRbig.*repmat( X_prj_big,1,1,3);
            writeVideo(videowriter,rescale(frameoD));
        else
            writeVideo(videowriter,blockproc(rescale(frameD),[1 1],fnroiexpand));
        end
        if(displayOn)
            figure(1);
            subplot(2,3,1);
            if saveHighdef
                imagesc(frameoD);
            else
                imagesc(blockproc(frameD,[ 1 1],fnroiexpand));
            end
            %imagesc(sPulseTrace(:,:,I));
            title(num2str(I));
            subplot(2,3,2);
            [PPGlag, PPG_peaksLoc] = getTauFromPPG(gtTimeWin,gtTraceWin, Fs_PPG);
            [rPPG_freq,rPPG_power] = fftrPPG(pulseTrace(startInd:endInd - maxLags), Fs);
            subplot(2,3,[4 5 6]);
            grid on;
            hold on;
            
            if I<startInd + stepSize - 1
                %t = Ts*double(I : I+1);
                t = timeTrace(I:I+1);
                x = pulseTrace(I:I+1);
                p=plot(t,x,'-o','Color',finalPlotColor,'MarkerSize',3);
                set(p,'MarkerFaceColor',finalPlotColor);
            end
            
            hold off
            pause(0.01);
        end
    end
    
    ind = ind + 1;
end

%Flush the remaining frames in OUT to disk
% for m =1:winLength
%     writeVideo(videowriter,mat2gray(OUT(:,:,:,m)));
% end
%
close(videowriter);
%save the pulsetrace
save([vidFolder 'pulseTrace'], 'pulseTrace','timeTrace','gtTime','gtTrace','sPulseTrace','SNRpx_all','SNRpx_win','HR_PPG','HR_rPPG_win','HR_rPPG_avg');
end









