function [E,F,X] = householder_r_post_temporal(E,F,r,k,col,wantx,X)
%%Find householder reflections to eliminate elements k+1:k+r-1 of column
%%col
%%NxN = block size and 1xS*S is the number of blocks
%%Total size of matrix is NxN*S*S
%%E is interlaced and folded to a NxNxS*S tensor

%normx = norm(E(k:k+1,col));
sz = size(E);
EE = E(k+1,col:col+r-1,:);
normx = sqrt(sum(EE.*EE));
normx = reshape(normx,1,1,size(E,3));

s = -sign(E(k+1,col+r-1,:));
u1 = E(k+1,col+r-1,:) - s.*normx;

% fprintf('HSHR_post : %2.4f - (%d)%2.4f = %2.4f\n',E(k+1,col+r-1),s,normx,u1);
w = [ E(k+1,col:col+r-2,:)./repmat(u1,1,numel(col+1:col+r-1)), ones(1,1,size(E,3))];

wt = mtimesx(w,'T',1);
tau = -s.*u1./normx;
wt_w = mtimesx(wt,w);
wt_w_tau = repmat(tau,size(wt_w,1),size(wt_w,1)).*wt_w;

E_W_TAU = mtimesx(E(:,col:col+r-1,:),wt_w_tau);
F_W_TAU = mtimesx(F(:,col:col+r-1,:),wt_w_tau);
% E := Qk*E
E(:,col:col+r-1,:) = E(:,col:col+r-1,:)-E_W_TAU;
% F := Qk*F
F(:,col:col+r-1,:) = F(:,col:col+r-1,:)-F_W_TAU;
if (wantx)
    X_W_TAU = mtimesx(tau,mtimesx(mtimesx(X(:,col:col+r-1,:),wt),w));
    X(:,col:col+r-1,:) = X(:,col:col+r-1,:)-X_W_TAU;
end

end